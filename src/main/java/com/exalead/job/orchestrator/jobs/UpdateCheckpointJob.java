package com.exalead.job.orchestrator.jobs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.exception.MAMIException.MAMI_EXCEPTION;
import com.exalead.job.orchestrator.mami.ConnectorsActionGroup;
import com.exalead.papi.helper.PushAPI;
import com.exalead.papi.helper.PushAPIException;
import com.exalead.papi.helper.PushAPIFactory;
import com.exalead.papi.helper.PushAPIVersion;



public class UpdateCheckpointJob implements Job {
	private static final Log logger = LogFactory.getLog(UpdateCheckpointJob.class);

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		logger.info("execute UpdateCheckpointJob..............");
		// Get host name
		String host = ctx.getJobDetail().getJobDataMap().getString("host");
		// Get Papi port
		String port = ctx.getJobDetail().getJobDataMap().getString("port");
		// Get the name of the checkpoint to update
		String name = ctx.getJobDetail().getJobDataMap().getString("name");
		// Get source (connector name)
		String source = ctx.getJobDetail().getJobDataMap().getString("source");
		// Get checkpointFormat PyyyyMMdd
		String checkpointFormat = ctx.getJobDetail().getJobDataMap().getString("CheckpointFormat");
		// check connectors status idle
		String[] connectorsNames = ctx.getJobDetail().getJobDataMap().getString("connectors_check_idle").split("[,;]");
		// Get gateway URL
		String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");
		try {
			MAMIClient client = MAMIClientFactory.build(gateway);
			// Case of connectors --> check that scan is not running
				// Consider running by default
				boolean bIdle = false;
				while (!bIdle){
					try {
						bIdle = ConnectorsActionGroup.areConnectorsIdle(client, connectorsNames);
					} catch (Exception e) {
						throw new JobExecutionException("Error getting connectors status for process [" + Arrays.toString(connectorsNames) + "]", e);
					}
	
					if (!bIdle) {
							long start=System.currentTimeMillis();
							while((System.currentTimeMillis()-start)<(new Long("60000"))){
						};
						logger.info("Will not restart process [" +  Arrays.toString(connectorsNames)
								+ "] because one of the connectors is running");
						throw new JobExecutionException("Will not restart process [" +  Arrays.toString(connectorsNames)
								+ "] because one of the connectors is running", new MAMIException(MAMI_EXCEPTION.GENERIC,
								"One of the connectors is running"));
					}
					
				}
			
			
			PushAPI papi = PushAPIFactory.createHttp(PushAPIVersion.PAPI_V4, host, port, source, "update_checkpoint", null,null);
			String nextCheckpoint = getNextCheckpoint(papi, name, checkpointFormat);
			updateCheckPoints(papi, name, nextCheckpoint);
		} catch (PushAPIException e) {
			logger.error("Error PushAPI", e);
			throw new JobExecutionException("Error PushAPI", e);
		} catch (ParseException e) {
			logger.error("Error parse checkPoint", e);
			throw new JobExecutionException("Error parse checkPoint", e);
		}
	}
	public void updateCheckPoints(PushAPI papi,String name,String value)throws PushAPIException {
		if (!StringUtils.isEmpty(name)) {
			if (!StringUtils.isEmpty(value)) {
				logger.info("Update checkpoint [" + name + "] with value [" + value + "]");
				papi.setCheckpoint(value, name, true);
			} else {
				logger.warn("CheckPoint value not specified !!!");
			}
		} else {
			logger.warn("CheckPoint name not specified !!!");
		}

		logger.info("DONE...");
	}
	
	public String getNextCheckpoint(PushAPI papi, String name,String checkpointFormat) throws PushAPIException, ParseException{
		String strCheckpoint = papi.getCheckpoint(name);
		logger.info("Previous value for checkpoint [" + name + "] --> [" + strCheckpoint + "]");
		logger.info("Checkpoint Format --> [" + checkpointFormat + "]");
		// CheckpointFormat :  yyyyMMdd
		DateFormat df = new SimpleDateFormat(checkpointFormat);
		Date date = df.parse(strCheckpoint);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		String nextCheckpoint = df.format(c.getTime());
		logger.info("Next value for checkpoint [" + name + "] --> [" + nextCheckpoint + "]");
		return nextCheckpoint;
	}
	
}
