package com.exalead.job.orchestrator.jobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.indexing.CompactThread;
import com.exalead.job.orchestrator.mami.ConnectorsActionGroup;
import com.exalead.job.orchestrator.mami.ReplicaActionGroup;
import com.exalead.job.orchestrator.mami.ReplicaActionGroup.Mode;
import com.exalead.toolbox.commons.DeploymentUtils;

public class FullCompactJob implements Job {

	private static Log logger = LogFactory.getLog(FullCompactJob.class);

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		// Get gateway URL
		String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");
		// Full compact threads (default value = 4)
		int compactThreads = 4;
		long compactTimeout = Long.MAX_VALUE;
		long slicesTimeout = Long.MAX_VALUE;
		if (ctx.getJobDetail().getJobDataMap().containsKey("compacthreads")) {
			compactThreads = ctx.getJobDetail().getJobDataMap().getInt("compacthreads");
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("compacttimeout")) {
			compactTimeout = ctx.getJobDetail().getJobDataMap().getLong("compacttimeout") * 1000l;
		}
		// Detach or not replicas
		boolean bDetachReplicas = ctx.getJobDetail().getJobDataMap().getBoolean("detach");
		// Don't run connectors if one of them is running
		boolean bCheckAllConnectorsIdle = true;
		if (ctx.getJobDetail().getJobDataMap().containsKey("checkconnectorsidle")) {
			bCheckAllConnectorsIdle = ctx.getJobDetail().getJobDataMap().getBoolean("checkconnectorsidle");
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("slicestimeout")) {
			slicesTimeout = ctx.getJobDetail().getJobDataMap().getLong("slicestimeout") * 1000l;
		}

		// By default --> consider that connectors are idle
		boolean connectorsIdle = true;

		String[] buildgroups = ctx.getJobDetail().getJobDataMap().getString("buildgroups").split("[,;]");

		// Sleep time
		long delay = 2000;

		MAMIClient client = MAMIClientFactory.build(gateway);
		List<String> connectorNames = ConnectorsActionGroup.getConnectorsList(client, new String[] { ".*" });

		if (bCheckAllConnectorsIdle) {
			try {
				connectorsIdle = ConnectorsActionGroup.areConnectorsIdle(client, connectorNames.toArray(new String[0]));
			} catch (MAMIException e) {
				logger.warn("Error getting connectors [" + connectorNames
						+ "] status (are idle) --> considering not idle...", e);
			}
		}

		if (connectorsIdle) {
			Map<String, Map<String, List<Integer>>> bgInstanceSlice = ReplicaActionGroup.gatherReplicaTargets(client);
			Map<String, List<Integer>> bgSlices = DeploymentUtils.getBGSlices(client.getMAMIManagers()
					.getDeploymentManager().getDeploymentConfig(), null, null, true);
			List<String> bgNames = getBGNames(bgSlices, buildgroups);

			// Detach slices before indexation
			if (bDetachReplicas) {
				if (bgInstanceSlice.size()>0){
					Date dStartDetach = new Date();
					long execTimeDetach = 0;

					logger.info("Detach replicas");
					for (String bgName : bgNames) {
						try {
							ReplicaActionGroup.manageBuildGroup(client, bgName, bgInstanceSlice.get(bgName), Mode.DETACH);
						} catch (Exception e) {
							logger.error("Error detach slave slice instance BG [" + bgName + "]", e);
						}
					}

					boolean bDetached = false;
					while (!bDetached) {
						Date dCurrent = new Date();
						execTimeDetach = dCurrent.getTime() - dStartDetach.getTime();
						if (execTimeDetach > slicesTimeout) {
							throw new JobExecutionException("Detach timeout reached (" + slicesTimeout + " ms)");
						}

						try {
							Thread.sleep(delay);
						} catch (InterruptedException e1) {
							logger.error("Error checking detach");
						}

						bDetached = ReplicaActionGroup.areSlicesDetached(bgInstanceSlice, bgNames, client);
					}

					logger.info("All slices detached");
				}else{
					logger.warn("No slice to detach (use build index)");
				}
			}

			ExecutorService executorCompact = Executors.newFixedThreadPool(compactThreads);
			for (String bgName : bgNames) {
				List<Integer> instanceSlices = bgSlices.get(bgName);
				for (int instanceSlice : instanceSlices) {
					Runnable worker = new CompactThread(bgName, instanceSlice, client, compactTimeout);
					executorCompact.execute(worker);
				}
			}

			executorCompact.shutdown();
			while (!executorCompact.isTerminated()) {
			}

			try {
				Thread.sleep(delay * 10);
			} catch (InterruptedException e) {
				throw new JobExecutionException("Error sleeping after full compact", e);
			}

			logger.info("Full compacts done ...");

			try {
				Thread.sleep(delay * 10);
			} catch (InterruptedException e) {
				throw new JobExecutionException("Error sleeping after clear slaves", e);
			}

			Date dStartAttach = new Date();
			long execTimeAttach = 0;

			boolean bAttach = false;
			if (bDetachReplicas) {
				bAttach = true;
				if (bgInstanceSlice.size()>0){
					logger.info("Attach replicas");
					for (String bgName : bgNames) {
						try {
							ReplicaActionGroup.manageBuildGroup(client, bgName, bgInstanceSlice.get(bgName), Mode.ATTACH);
						} catch (Exception e) {
							logger.error("Error attach slave slice instance BG [" + bgName + "]", e);
						}
					}
				}else{
					bAttach = false;
					logger.warn("No slice to attach (use build index)");
				}
			}

			if (bAttach) {
				boolean bAttached = false;

				while (!bAttached) {
					Date dCurrent = new Date();
					execTimeAttach = dCurrent.getTime() - dStartAttach.getTime();
					if (execTimeAttach > slicesTimeout) {
						throw new JobExecutionException("Attach timeout reached (" + slicesTimeout + " ms)");
					}

					try {
						Thread.sleep(delay);
					} catch (InterruptedException e1) {
						logger.error("Error checking attach");
					}

					bAttached = ReplicaActionGroup.areSlicesAttached(bgInstanceSlice, bgNames, client);
				}
			}
		} else {
			logger.warn("One of connectors [" + connectorNames + "] not idle --> will not execute full compact");
		}
	}

	private static List<String> getBGNames(Map<String, List<Integer>> bgInstanceSlice, String[] bgRegExps) {
		logger.debug("Get build groups to process for slices [" + bgInstanceSlice + "] and regular expression(s) "
				+ Arrays.toString(bgRegExps));
		List<String> bgNames = new ArrayList<>();

		for (String regexp : bgRegExps) {
			logger.debug("Find BGs for regular expression [" + regexp + "]");
			for (String bgName : bgInstanceSlice.keySet()) {
				if (bgName.matches(regexp) && !bgNames.contains(bgName)) {
					logger.debug("Add BG [" + bgName + "]");
					bgNames.add(bgName);
				} else {
					logger.debug("Don't add BG [" + bgName + "]");
				}
			}
		}

		logger.debug("Found build groups to compact [" + bgNames + "]");
		return bgNames;
	}
}
