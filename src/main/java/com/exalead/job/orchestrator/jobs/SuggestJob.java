package com.exalead.job.orchestrator.jobs;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.mami.SearchActionGroup;

public class SuggestJob implements Job {

    private static Log logger = LogFactory.getLog(SuggestJob.class);

    @Override
    public void execute(JobExecutionContext ctx) throws JobExecutionException {

        // Get gateway URL
        String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");

        long suggestTimeout = Long.MAX_VALUE;
        if (ctx.getJobDetail().getJobDataMap().containsKey("suggesttimeout")) {
            suggestTimeout = ctx.getJobDetail().getJobDataMap().getLong("suggesttimeout") * 1000l;
        }

        MAMIClient client = MAMIClientFactory.build(gateway);
        List<String> suggestList = null;

        // Sleep time
        long delay = 2000;

        if (ctx.getJobDetail().getJobDataMap().containsKey("suggests")) {
            suggestList =
                    SearchActionGroup.getSuggestsList(client, ctx.getJobDetail().getJobDataMap()
                            .getString("suggests").split("[,;]"));
        }

        if (suggestList != null && suggestList.size() > 0) {
            boolean jobIdle = SearchActionGroup.areSuggestIdle(client, suggestList);
            if (jobIdle) {
                Date dStartSuggest = new Date();
                long execTimeSuggest = 0;
                for (String suggestName : suggestList) {
                    try {
                        logger.info("Generate suggest [" + suggestName + "]");
                        SearchActionGroup.buildSuggest(client, suggestName);
                        boolean bIdle = false;

                        while (!bIdle) {
                            Date dCurrent = new Date();
                            execTimeSuggest = dCurrent.getTime() - dStartSuggest.getTime();
                            if (execTimeSuggest > suggestTimeout) {
                                throw new JobExecutionException("Suggest timeout reached ("
                                        + suggestTimeout + " ms)");
                            }

                            try {
                                Thread.sleep(delay * 10);
                                bIdle = SearchActionGroup.areSuggestIdle(client, suggestList);
                                if (bIdle) {
                                    logger.info("suggest Generated for [" + suggestName + "]");
                                }
                            } catch (Exception e) {
                                throw new JobExecutionException("Error Checking suggest status", e);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Error generating suggest [" + suggestName + "]", e);
                    }
                }

                logger.info("Suggests generated");
            }
        }
    }

}
