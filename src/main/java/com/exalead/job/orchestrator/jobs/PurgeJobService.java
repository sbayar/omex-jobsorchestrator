package com.exalead.job.orchestrator.jobs;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class PurgeJobService implements Job {
    private static final Log logger = LogFactory.getLog(PurgeJobService.class);

    @Override
    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        logger.info("Purge job executed on " + new Date());
        String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway").trim();
        try {
            JSONArray array = this.getJobsToPurge(gateway);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                if (object.getInt("duration") > 0) {
                    this.purgeJob(object, gateway);
                }
            }
        } catch (Exception e) {
            logger.error("Purge job error: " + e.getMessage());
        }
    }

    // HTTP GET request
    private JSONArray getJobsToPurge(String gateway) throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet get = new HttpGet(gateway + "/purgeJob/all");
        get.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(get);
        BufferedReader rd =
                new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        HttpClientUtils.closeQuietly(response);
        HttpClientUtils.closeQuietly(client);
        get.releaseConnection();
        logger.info("GetJobsToPurge result: " + result.toString());
        return new JSONArray(result.toString());
    }

    // HTTP POST request
    private void purgeJob(JSONObject obj, String gateway) throws Exception {
        HttpClient client = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(gateway + "/purgeJob/purge");
        HttpResponse response = null;
        try {
            StringEntity entity = new StringEntity(obj.toString(), Consts.UTF_8.name());
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            logger.info("executing request for  " + obj.toString() + " : "
                    + httpPost.getRequestLine());
            response = client.execute(httpPost);
            HttpEntity resEntity = response.getEntity();

            logger.info("----------------------------------------");
            logger.info(response.getStatusLine());
            if (resEntity != null) {
                String responseBody = EntityUtils.toString(resEntity);
                logger.info("Data: " + responseBody);
            }
            EntityUtils.consume(resEntity);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            HttpClientUtils.closeQuietly(response);
            HttpClientUtils.closeQuietly(client);
            httpPost.releaseConnection();
        }
    }
}
