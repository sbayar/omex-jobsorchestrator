package com.exalead.job.orchestrator.jobs;


import com.exalead.searchapi.xmlv10.client.SearchParameter;
import com.exalead.searchapi.xmlv10.client.SearchQuery;

public class QueryUtils {
	public static void addValueFacet(SearchQuery searchQuery, String id, String field, boolean exhaustive) {
		searchQuery.addParameter("f." + id + ".type", "value");
		searchQuery.addParameter("f." + id + ".vroot", "Top/" + id);
		searchQuery.addParameter("f." + id + ".field", field);
		if (exhaustive) {
			searchQuery.addParameter("f." + id + ".max_per_level", "0");
		}
	}

	public static void addValueFacet(SearchQuery searchQuery, String field, boolean exhaustive) {
		addValueFacet(searchQuery, field, field, exhaustive);
	}

	public static void setSearchLogic(SearchQuery searchQuery, String logic) {
		searchQuery.setSearchLogic(logic);
	}

	public static void setSearchTarget(SearchQuery searchQuery, String target) {
		searchQuery.setTarget(target);
	}

	public static void setSearchLogicAndTarget(SearchQuery searchQuery, String logic, String target) {
		setSearchLogic(searchQuery, logic);
		setSearchTarget(searchQuery, target);
	}

	public static void addValueFacet(SearchQuery searchQuery, String field) {
		addValueFacet(searchQuery, field, field, true);
	}

	public static void addValueFacet(SearchQuery searchQuery, String id, String field) {
		addValueFacet(searchQuery, id, field, true);
	}

	public static void add2DFacet(SearchQuery searchQuery, String id1, String id2, String id, boolean exhaustive) {
		searchQuery.addParameter("f." + id + ".type", "2D");
		searchQuery.addParameter("f." + id + ".vroot", "Top/" + id);
		searchQuery.addParameter("f." + id + ".id1", id1);
		searchQuery.addParameter("f." + id + ".id2", id2);
		searchQuery.addParameter("f." + id + ".ds", "false");
		if (exhaustive) {
			searchQuery.addParameter("f." + id + ".max_per_level", "0");
		}
	}

	public static void add2DFacet(SearchQuery searchQuery, String id1, String id2, String id) {
		add2DFacet(searchQuery, id1, id2, id, true);
	}

	public static void addAggregation(SearchQuery searchQuery, String id, String aggName, String aggFunction) {
		searchQuery.addParameter("f." + id + ".aggr." + aggName, aggFunction);
	}

	public static void addSortAggregation(SearchQuery searchQuery, String id, String aggName) {
		searchQuery.addParameter("f." + id + ".sort", "aggregation");
		searchQuery.addParameter("f." + id + ".sort_agg_fun", aggName);
	}

	public static void disableStandardFacets(SearchQuery searchQuery) {
		searchQuery.addParameter(SearchParameter.USE_LOGIC_FACETS, "false");
	}

	public static void disableSynthesis(SearchQuery searchQuery) {
		searchQuery.addParameter(SearchParameter.SYNTHESIS, "disabled");
	}

	public static void disableStandardMetas(SearchQuery searchQuery) {
		searchQuery.addParameter(SearchParameter.USE_LOGIC_HIT_METAS, "false");
	}

	public static void addPerformanceOptions(SearchQuery searchQuery) {
		searchQuery.addParameter(SearchParameter.RELEVANCE, "false");
		searchQuery.addParameter(SearchParameter.TIMEOUT, "0");
		searchQuery.addParameter(SearchParameter.SORTED, "false");
		searchQuery.addParameter(SearchParameter.STREAMING, "true");
		searchQuery.addParameter(SearchParameter.OUTPUT_FORMAT, "flea");
		searchQuery.addParameter(SearchParameter.USE_LOGIC_FACETS, "false");
		searchQuery.addParameter(SearchParameter.USE_LOGIC_HIT_METAS, "false");
		searchQuery.addParameter(SearchParameter.USE_LOGIC_VIRTUAL_FIELDS,"false");
		disableSynthesis(searchQuery);
	}

	public static void configureStreamedFullHits(SearchQuery searchQuery) {
		searchQuery.addParameter(SearchParameter.STREAMING, "true");
		addPerformanceOptions(searchQuery);
		disableSynthesis(searchQuery);
		disableStandardFacets(searchQuery);
		searchQuery.setResultsSetLength(-1);
	}

	public static void disableStandardHitMetas(SearchQuery searchQuery) {
		searchQuery.addParameter(SearchParameter.USE_LOGIC_HIT_METAS, "false");
	}

	public static void addMetaFromCSV(SearchQuery searchQuery, String field, String metaName) {
		searchQuery.addParameter(SearchParameter.ADD_HIT_META, "name:" + metaName + ",multi_field:" + field);
	}

	public static void addMetaFromField(SearchQuery searchQuery, String field, String metaName) {
		searchQuery.addParameter(SearchParameter.ADD_HIT_META, "name:" + metaName + ",index_field:" + field);
	}

	public static void configureStreamedFullHitsForMeta(SearchQuery searchQuery, String field, String metaName) {
		searchQuery.addParameter(SearchParameter.STREAMING, "true");
		addPerformanceOptions(searchQuery);
		disableSynthesis(searchQuery);
		disableStandardFacets(searchQuery);
		searchQuery.setResultsSetLength(-1);
		disableStandardHitMetas(searchQuery);
		addMetaFromField(searchQuery, field, metaName);
	}

	public static void addDynamicSearchTarget(SearchQuery searchQuery, String bgName) {
		searchQuery.addParameter("st", "dyn_" + bgName);
		searchQuery.addParameter("st.dyn_" + bgName + ".type", "local");
		searchQuery.addParameter("st.dyn_" + bgName + ".build_groups", bgName);
	}
}
