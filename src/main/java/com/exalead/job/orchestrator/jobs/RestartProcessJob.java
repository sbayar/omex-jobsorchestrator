package com.exalead.job.orchestrator.jobs;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.exception.MAMIException.MAMI_EXCEPTION;
import com.exalead.job.orchestrator.mami.ConnectorsActionGroup;
import com.exalead.job.orchestrator.mami.ProcessActionGroup;



public class RestartProcessJob implements Job {
	private static final Log logger = LogFactory.getLog(RestartProcessJob.class);

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		logger.info("execute RestartProcessJob..............");
		// Get process names to restart
		String[] processNames = ctx.getJobDetail().getJobDataMap().getString("processes").split("[,;]");
		// check connectors status idle
		String[] connectorsNames = ctx.getJobDetail().getJobDataMap().getString("connectors_check_idle").split("[,;]");
		
		// Get host name
		String hostName = ctx.getJobDetail().getJobDataMap().getString("host");
		// Get gateway URL
		String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");
		String instance = "cvdefault";
		if (ctx.getJobDetail().getJobDataMap().containsKey("instance")) {
			instance = ctx.getJobDetail().getJobDataMap().getString("instance");
		}
		long timeout = 60000;
		if (ctx.getJobDetail().getJobDataMap().containsKey("timeout")) {
			timeout = 1000l * ctx.getJobDetail().getJobDataMap().getLong("timeout");
		}

		MAMIClient client = MAMIClientFactory.build(gateway);

		
			// Case of connectors --> check that scan is not running
				// Consider running by default
				boolean bIdle = false;
				while (!bIdle){
					try {
						bIdle = ConnectorsActionGroup.areConnectorsIdle(client, connectorsNames);
					} catch (Exception e) {
						throw new JobExecutionException("Error getting connectors status for process [" + Arrays.toString(connectorsNames) + "]", e);
					}
	
					if (!bIdle) {
							long start=System.currentTimeMillis();
							while((System.currentTimeMillis()-start)<(new Long("60000"))){
						};
						logger.info("Will not restart process [" +  Arrays.toString(connectorsNames)
								+ "] because one of the connectors is running");
						throw new JobExecutionException("Will not restart process [" +  Arrays.toString(connectorsNames)
								+ "] because one of the connectors is running", new MAMIException(MAMI_EXCEPTION.GENERIC,
								"One of the connectors is running"));
					}
					
				}
				for (String process : processNames) {

			try {
				ProcessActionGroup.restartProcess(hostName, instance, process, client, timeout);
			} catch (MAMIException e) {
				logger.error("Error restarting process [" + process + "]", e);
				throw new JobExecutionException("Error restarting process [" + process + "]", e);
			}
		}
	}
}
