package com.exalead.job.orchestrator.jobs;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.connectors.ScanThread;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.indexing.CompactThread;
import com.exalead.job.orchestrator.indexing.IndexingThread;
import com.exalead.job.orchestrator.mami.ConnectorsActionGroup;
import com.exalead.job.orchestrator.mami.IndexingActionGroup;
import com.exalead.job.orchestrator.mami.ReplicaActionGroup;
import com.exalead.job.orchestrator.mami.ReplicaActionGroup.Mode;
import com.exalead.job.orchestrator.mami.SearchActionGroup;

public class ConnectorsFullScan implements Job {

	private static Log logger = LogFactory.getLog(ConnectorsFullScan.class);

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		// Get all connectors to be synchronized (can be regular expressions)
		String[] connectorNamesRegexp = ctx.getJobDetail().getJobDataMap().getString("connectors").split("[,;]");
		// clean or not connector before scan
		boolean bClearMaster = ctx.getJobDetail().getJobDataMap().getBoolean("clean");
		// clean buildgroups before scan
		boolean bClearBGs = ctx.getJobDetail().getJobDataMap().getBoolean("cleanbgs");
		// clean slave slices before attach
		boolean bClearSlaves = ctx.getJobDetail().getJobDataMap().getBoolean("cleanslaves");
		// detach or not replicas
		boolean bDetachReplicas = ctx.getJobDetail().getJobDataMap().getBoolean("detach");
		// compact master after indexation
		boolean bCompact = ctx.getJobDetail().getJobDataMap().getBoolean("compact");
		// Get gateway URL
		String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");
		// Connector thread pool size
		int connectorThreads = ctx.getJobDetail().getJobDataMap().getInt("connectorthreads");
		// Analysis thread pool size
		int analyzeThreads = ctx.getJobDetail().getJobDataMap().getInt("analyzethreads");
		// Full compact threads (default value = 4)
		int compactThreads = 4;
		// Trigger indexing job
		boolean bIndex = ctx.getJobDetail().getJobDataMap().getBoolean("triggerindexing");
		// Timeout for connector thread
		long connectorTimeout = Long.MAX_VALUE;
		long slicesTimeout = Long.MAX_VALUE;
		long clearTimeout = Long.MAX_VALUE;
		long indexTimeout = Long.MAX_VALUE;
		long compactTimeout = Long.MAX_VALUE;
		long suggestTimeout = Long.MAX_VALUE;
		if (ctx.getJobDetail().getJobDataMap().containsKey("connectortimeout")) {
			connectorTimeout = ctx.getJobDetail().getJobDataMap().getLong("connectortimeout") * 1000l;
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("slicestimeout")) {
			slicesTimeout = ctx.getJobDetail().getJobDataMap().getLong("slicestimeout") * 1000l;
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("cleartimeout")) {
			clearTimeout = ctx.getJobDetail().getJobDataMap().getLong("cleartimeout") * 1000l;
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("indextimeout")) {
			indexTimeout = ctx.getJobDetail().getJobDataMap().getLong("indextimeout") * 1000l;
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("compacttimeout")) {
			compactTimeout = ctx.getJobDetail().getJobDataMap().getLong("compacttimeout") * 1000l;
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("suggesttimeout")) {
			suggestTimeout = ctx.getJobDetail().getJobDataMap().getLong("suggesttimeout") * 1000l;
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("compacthreads")) {
			compactThreads = ctx.getJobDetail().getJobDataMap().getInt("compacthreads");
		}
		// Don't run connectors if one of them is running
		boolean bCheckAllConnectorsIdle = true;
		if (ctx.getJobDetail().getJobDataMap().containsKey("checkconnectorsidle")) {
			bCheckAllConnectorsIdle = ctx.getJobDetail().getJobDataMap().getBoolean("checkconnectorsidle");
		}

		// By default --> consider that connectors are idle
		boolean connectorsIdle = true;

		// Sleep time
		long delay = 2000;

		MAMIClient client = MAMIClientFactory.build(gateway);
		List<String> connectorNames = ConnectorsActionGroup.getConnectorsList(client, connectorNamesRegexp);

		if (bCheckAllConnectorsIdle) {
			try {
				connectorsIdle = !ConnectorsActionGroup.areConnectorsWorking(client, connectorNames.toArray(new String[0]));
			} catch (MAMIException e) {
				logger.warn("Error getting connectors [" + connectorNames
						+ "] status (are idle) --> considering not idle...", e);
			}
		}

		if (connectorsIdle) {
			List<String> connectorsBGs = ConnectorsActionGroup.getConnectorsBGs(client, connectorNamesRegexp);

			List<String> suggestList = null;
			if (ctx.getJobDetail().getJobDataMap().containsKey("suggests")) {
				suggestList = SearchActionGroup.getSuggestsList(client,
						ctx.getJobDetail().getJobDataMap().getString("suggests").split("[,;]"));
			}

			Map<String, Map<String, List<Integer>>> bgInstanceSlice = ReplicaActionGroup.gatherReplicaTargets(client);

			// Detach slices before indexation
			if (bDetachReplicas) {
				Date dStartDetach = new Date();
				long execTimeDetach = 0;

				logger.info("Detach replicas");
				for (String bgName : connectorsBGs) {
					try {
						ReplicaActionGroup.manageBuildGroup(client, bgName, bgInstanceSlice.get(bgName), Mode.DETACH);
					} catch (Exception e) {
						logger.error("Error detach slave slice instance BG [" + bgName + "]", e);
					}
				}

				boolean bDetached = false;
				while (!bDetached) {
					Date dCurrent = new Date();
					execTimeDetach = dCurrent.getTime() - dStartDetach.getTime();
					if (execTimeDetach > slicesTimeout) {
						throw new JobExecutionException("Detach timeout reached (" + slicesTimeout + " ms)");
					}

					try {
						Thread.sleep(delay);
					} catch (InterruptedException e1) {
						logger.error("Error checking detach");
					}

					bDetached = ReplicaActionGroup.areSlicesDetached(bgInstanceSlice, connectorsBGs, client);
				}

				logger.info("All slices detached");
			}

			if (bClearBGs) {
				Date dStartClear = new Date();
				long execTimeClear = 0;

				logger.info("Clear master slices for BGs [" + connectorsBGs + "]");
				for (String bgName : connectorsBGs) {
					IndexingActionGroup.clearMasterBuildGroup(bgName, client);
				}

				boolean bEmpty = false;
				while (!bEmpty) {
					Date dCurrent = new Date();
					execTimeClear = dCurrent.getTime() - dStartClear.getTime();
					if (execTimeClear > clearTimeout) {
						throw new JobExecutionException("Clear BG timeout reached (" + clearTimeout + " ms)");
					}

					try {
						Thread.sleep(delay * 10);
						bEmpty = IndexingActionGroup.isBGMasterEmpty(connectorsBGs, client);
					} catch (Exception e) {
						throw new JobExecutionException("Error Checking empty BGs", e);
					}
				}
				logger.info("Build groups empty [" + connectorsBGs + "]");
			}

			if (bClearMaster && !bClearBGs) {
				logger.info("Clear connectors [" + connectorNames + "]");
				Date dStartClear = new Date();
				long execTimeClear = 0;

				for (String connectorName : connectorNames) {
					ConnectorsActionGroup.clearConnector(connectorName, client);
				}

				boolean bEmpty = false;
				try {
					bEmpty = ConnectorsActionGroup.isConnectorsEmpty(connectorNames, client);
				} catch (MAMIException e) {
					throw new JobExecutionException("Error getting connectors documents count", e);
				}

				while (!bEmpty) {
					Date dCurrent = new Date();
					execTimeClear = dCurrent.getTime() - dStartClear.getTime();
					if (execTimeClear > clearTimeout) {
						throw new JobExecutionException("Clear Connectors timeout reached (" + clearTimeout + " ms)");
					}

					try {
						Thread.sleep(delay);
						bEmpty = ConnectorsActionGroup.isConnectorsEmpty(connectorNames, client);
					} catch (Exception e) {
						throw new JobExecutionException("Error getting connectors documents count", e);
					}
				}
				logger.info("Connectors empty ...");
			}

			logger.info("Full scan connectors [" + connectorNames + "]");
			ExecutorService executor = Executors.newFixedThreadPool(connectorThreads);
			for (String connectorName : connectorNames) {
					Runnable worker = new ScanThread(connectorName, client, connectorTimeout);
					executor.execute(worker);
			}

			executor.shutdown(); //  JBU initiate shutdown
			try {
                while (!executor.awaitTermination(5, TimeUnit.SECONDS)) {
                }
            } catch (InterruptedException e) {
                throw new JobExecutionException("Error while waiting for full scan executor effective termination", e);                    
            }

			// JBU
			logger.info("Full scan connectors done.");

			try {
				Thread.sleep(10000 /* delay * 10 */ /* JBU */);
			} catch (InterruptedException e) {
				throw new JobExecutionException("Error sleeping after full scan", e);
			}

			if (bIndex) {
				executor = Executors.newFixedThreadPool(analyzeThreads);
				for (String bg : connectorsBGs) {
					Runnable worker = new IndexingThread(bg, client, indexTimeout);
					executor.execute(worker);
				}

				executor.shutdown();
				while (!executor.isTerminated()) {
				}
			}

			logger.info("All indexations done ...");

			if (bCompact) {
				ExecutorService executorCompact = Executors.newFixedThreadPool(compactThreads);
				for (String bgName : connectorsBGs) {
					Map<String, List<Integer>> bgInstances = bgInstanceSlice.get(bgName);
					for (String instanceName : bgInstances.keySet()) {
						List<Integer> instanceSlices = bgInstances.get(instanceName);
						for (int instanceSlice : instanceSlices) {
							Runnable worker = new CompactThread(bgName, instanceSlice, client, compactTimeout);
							executorCompact.execute(worker);
						}
					}
				}

				executorCompact.shutdown();
				while (!executorCompact.isTerminated()) {
				}

				try {
					Thread.sleep(delay * 10);
				} catch (InterruptedException e) {
					throw new JobExecutionException("Error sleeping after full compact", e);
				}

				logger.info("Full compacts done ...");
			}

			if (bClearSlaves) {
				Date dStartClear = new Date();
				long execTimeClear = 0;

				for (String bgName : connectorsBGs) {
					try {
						ReplicaActionGroup.manageBuildGroup(client, bgName, bgInstanceSlice.get(bgName),
								Mode.CLEARREPLICA);
					} catch (Exception e) {
						logger.error("Error clearing slave slice instance BG [" + bgName + "]", e);
					}
				}

				boolean bSlavesCleaned = false;
				while (!bSlavesCleaned) {
					Date dCurrent = new Date();
					execTimeClear = dCurrent.getTime() - dStartClear.getTime();
					if (execTimeClear > clearTimeout) {
						throw new JobExecutionException("Clear slave timeout reached (" + clearTimeout + " ms)");
					}

					try {
						Thread.sleep(delay * 10);
						bSlavesCleaned = ReplicaActionGroup.areSlicesCleaned(bgInstanceSlice, connectorsBGs, client);
					} catch (Exception e) {
						throw new JobExecutionException("Error Checking empty BGs slave slices", e);
					}
				}
				logger.info("Build groups slave instances empty [" + connectorsBGs + "]");
			}

			try {
				Thread.sleep(delay * 10);
			} catch (InterruptedException e) {
				throw new JobExecutionException("Error sleeping after clear slaves", e);
			}

			Date dStartAttach = new Date();
			long execTimeAttach = 0;

			if (bDetachReplicas) {
				logger.info("Attach replicas");
				for (String bgName : connectorsBGs) {
					try {
						ReplicaActionGroup.manageBuildGroup(client, bgName, bgInstanceSlice.get(bgName), Mode.ATTACH);
					} catch (Exception e) {
						logger.error("Error attach slave slice instance BG [" + bgName + "]", e);
					}
				}
			}

			if (bDetachReplicas) {
				boolean bAttached = false;

				while (!bAttached) {
					Date dCurrent = new Date();
					execTimeAttach = dCurrent.getTime() - dStartAttach.getTime();
					if (execTimeAttach > slicesTimeout) {
						throw new JobExecutionException("Attach timeout reached (" + slicesTimeout + " ms)");
					}

					try {
						Thread.sleep(delay);
					} catch (InterruptedException e1) {
						logger.error("Error checking attach");
					}

					bAttached = ReplicaActionGroup.areSlicesAttached(bgInstanceSlice, connectorsBGs, client);
				}
			}

			if (suggestList != null && suggestList.size() > 0) {
				Date dStartSuggest = new Date();
				long execTimeSuggest = 0;
				for (String suggestName : suggestList) {
					try {
						logger.info("Generate suggest [" + suggestName + "]");
						SearchActionGroup.buildSuggest(client, suggestName);
					} catch (Exception e) {
						logger.error("Error generating suggest [" + suggestName + "]", e);
					}
				}

				boolean bIdle = false;

				while (!bIdle) {
					Date dCurrent = new Date();
					execTimeSuggest = dCurrent.getTime() - dStartSuggest.getTime();
					if (execTimeSuggest > suggestTimeout) {
						throw new JobExecutionException("Suggest timeout reached (" + suggestTimeout + " ms)");
					}

					try {
						Thread.sleep(delay * 10);
						bIdle = SearchActionGroup.areSuggestIdle(client, suggestList);
					} catch (Exception e) {
						throw new JobExecutionException("Error Checking suggest status", e);
					}
				}

				logger.info("Suggests generated");
			}
		} else {
			logger.warn("One of connectors [" + connectorNames + "] not idle --> will not execute full scan");
		}
	}
	
	
	public static String defaultValue(String path, String fileName) {
        String result = "";
        try {
            Properties prop = new Properties();
            String propFileName = fileName;
            InputStream inputStream = new FileInputStream(propFileName);
            if (inputStream != null) {
                prop.load(inputStream);
            }
            // get the property value and print it out
            result = prop.getProperty(path);
            inputStream.close();
            return result;
        } catch (Exception e) {
        	logger.error("Exception: " + e);
        } finally {
            // inputStream.close();
        }
        return null;
    }
	
}
