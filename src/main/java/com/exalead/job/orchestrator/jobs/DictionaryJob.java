package com.exalead.job.orchestrator.jobs;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.mami.DictionaryActionGroup;

public class DictionaryJob implements Job {

    private static Log logger = LogFactory.getLog(DictionaryJob.class);

    @Override
    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        logger.info("Begin Conpact and build dictionary.");
        // Get gateway URL
        String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");

        long dictionaryTimeout = Long.MAX_VALUE;
        if (ctx.getJobDetail().getJobDataMap().containsKey("dictionarytimeout")) {
            dictionaryTimeout =
                    ctx.getJobDetail().getJobDataMap().getLong("dictionarytimeout") * 1000l;
        }
        long delay = 2000;

        MAMIClient client = MAMIClientFactory.build(gateway);
        List<String> dictionaryList = null;

        if (ctx.getJobDetail().getJobDataMap().containsKey("dictionaries")) {
            dictionaryList =
                    DictionaryActionGroup.getDictionaryList(client, ctx.getJobDetail()
                            .getJobDataMap().getString("dictionaries").split("[,;]"));
        }
        logger.info("Dictionaries size : "+dictionaryList.size() );

        if (dictionaryList != null && dictionaryList.size() > 0) {
            Date dStartDictionary = new Date();
            long execTimeDictionary = 0;
            boolean jobIdle = DictionaryActionGroup.areDictionaryIdle(client, dictionaryList);
            if (jobIdle) {
                logger.info("Dictionaries execute." );
                for (String dictionaryName : dictionaryList) {
                    try {
                        logger.info("Conpact and build dictionary [" + dictionaryName + "]");
                        DictionaryActionGroup.compactAndBuildDictionary(client, dictionaryName);
                        boolean bIdle = false;
                        while (!bIdle) {
                            Date dCurrent = new Date();
                            execTimeDictionary = dCurrent.getTime() - dStartDictionary.getTime();
                            if (execTimeDictionary > dictionaryTimeout) {
                                throw new JobExecutionException("Duggest timeout reached ("
                                        + dictionaryTimeout + " ms)");
                            }

                            try {
                                Thread.sleep(delay * 10);
                                bIdle =
                                        DictionaryActionGroup.areDictionaryIdle(client,
                                                dictionaryList);
                                if (bIdle) {
                                    logger.info("dictionary Generated for [" + dictionaryName + "]");
                                }
                            } catch (Exception e) {
                                throw new JobExecutionException("Error Checking dictionary status",
                                        e);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Error conpact And build dictionary [" + dictionaryName + "]",
                                e);
                    }
                }

                logger.info("Dictionaries generated");
            }
        }
    }

}
