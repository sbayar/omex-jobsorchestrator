package com.exalead.job.orchestrator.jobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.indexing.IndexingThread;
import com.exalead.job.orchestrator.mami.ConnectorsActionGroup;
import com.exalead.job.orchestrator.mami.IndexingActionGroup;
import com.exalead.toolbox.commons.DeploymentUtils;

public class TriggerIndexingJob implements Job {

	private static Log logger = LogFactory.getLog(TriggerIndexingJob.class);

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		// Get gateway URL
		String gateway = ctx.getJobDetail().getJobDataMap().getString("gateway");
		// Commit threads (default value = 4)
		int commitThreads = 4;
		long commitTimeout = Long.MAX_VALUE;
		if (ctx.getJobDetail().getJobDataMap().containsKey("committhreads")) {
			commitThreads = ctx.getJobDetail().getJobDataMap().getInt("committhreads");
		}
		if (ctx.getJobDetail().getJobDataMap().containsKey("committimeout")) {
			commitTimeout = ctx.getJobDetail().getJobDataMap().getLong("committimeout") * 1000l;
		}

		String[] buildgroups = ctx.getJobDetail().getJobDataMap().getString("buildgroups").split("[,;]");

		// Don't run connectors if one of them is running
		boolean bCheckAllConnectorsIdle = true;
		if (ctx.getJobDetail().getJobDataMap().containsKey("checkconnectorsidle")) {
			bCheckAllConnectorsIdle = ctx.getJobDetail().getJobDataMap().getBoolean("checkconnectorsidle");
		}

		// By default --> consider that connectors are idle
		boolean connectorsIdle = true;

		// Sleep time
		long delay = 2000;

		MAMIClient client = MAMIClientFactory.build(gateway);
		List<String> connectorNames = ConnectorsActionGroup.getConnectorsList(client, new String[] { ".*" });

		if (bCheckAllConnectorsIdle) {
			try {
				connectorsIdle = !ConnectorsActionGroup.areConnectorsWorking(client, connectorNames.toArray(new String[0]));
			} catch (MAMIException e) {
				logger.warn("Error getting connectors [" + connectorNames
						+ "] status (are idle) --> considering not idle...", e);
			}
		}

		if (connectorsIdle) {
			Map<String, List<Integer>> bgSlices = DeploymentUtils.getBGSlices(client.getMAMIManagers()
					.getDeploymentManager().getDeploymentConfig(), null, null, true);
			List<String> bgNames = getBGNames(bgSlices, buildgroups);

			// Trigger commit
			ExecutorService executor = Executors.newFixedThreadPool(commitThreads);
			for (String bg : bgNames) {
				Runnable worker = new IndexingThread(bg, client, commitTimeout);
				executor.execute(worker);
			}

			executor.shutdown();
			while (!executor.isTerminated()) {
			}

			boolean bIndexing = true;
			Date dStartIndexing = new Date();
			long execTimeIndexing = 0;

			while (bIndexing) {
				Date dCurrent = new Date();
				execTimeIndexing = dCurrent.getTime() - dStartIndexing.getTime();
				if (execTimeIndexing > commitTimeout) {
					throw new JobExecutionException("Indexing timeout reached (" + commitTimeout + " ms)");
				}

				try {
					Thread.sleep(delay);
					bIndexing = IndexingActionGroup.isIndexingOrCompacting(client, bgNames);
				} catch (Exception e) {
					throw new JobExecutionException("Error while checking indexing status", e);
				}
			}

			logger.info("Commit done for buildgroups [" + StringUtils.join(bgNames, ";"));
		} else {
			logger.warn("One of connectors [" + connectorNames + "] not idle --> will not execute commit Job");
		}
	}

	private static List<String> getBGNames(Map<String, List<Integer>> bgInstanceSlice, String[] bgRegExps) {
		logger.debug("Get build groups to process for slices [" + bgInstanceSlice + "] and regular expression(s) "
				+ Arrays.toString(bgRegExps));
		List<String> bgNames = new ArrayList<>();

		for (String regexp : bgRegExps) {
			logger.debug("Find BGs for regular expression [" + regexp + "]");
			for (String bgName : bgInstanceSlice.keySet()) {
				if (bgName.matches(regexp) && !bgNames.contains(bgName)) {
					logger.debug("Add BG [" + bgName + "]");
					bgNames.add(bgName);
				} else {
					logger.debug("Don't add BG [" + bgName + "]");
				}
			}
		}

		logger.debug("Found build groups to compact [" + bgNames + "]");
		return bgNames;
	}
}
