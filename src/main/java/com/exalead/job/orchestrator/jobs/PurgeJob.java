package com.exalead.job.orchestrator.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.exalead.papi.helper.EnumerationMode;
import com.exalead.papi.helper.PushAPI;
import com.exalead.papi.helper.PushAPIException;
import com.exalead.papi.helper.PushAPIFactory;
import com.exalead.papi.helper.PushAPIVersion;
import com.exalead.papi.helper.SyncedEntry;

public class PurgeJob implements Job {
    private static final Log logger = LogFactory.getLog(PurgeJob.class);
    private static final String PAPI_DATE_FORMAT_IN_URI_TREE = "yyyy/MM/dd";
    private String papiIP;
    private String papiPort;
    private String connectorName;
    private String papiLogin;
    private String papiPassword;
    private int duration = 200;

    @Override
    public void execute(JobExecutionContext ctx) throws JobExecutionException {
        logger.info("Purge job executed on " + new Date());
        papiIP = ctx.getJobDetail().getJobDataMap().getString("papiIP").trim();
        papiPort = ctx.getJobDetail().getJobDataMap().getString("papiPort");
        connectorName = ctx.getJobDetail().getJobDataMap().getString("connectorName").trim();
        papiLogin = ctx.getJobDetail().getJobDataMap().getString("papiLogin").trim();
        papiPassword = ctx.getJobDetail().getJobDataMap().getString("papiPassword").trim();
        duration = ctx.getJobDetail().getJobDataMap().getInt("olderThanXDays");
        if (papiPort.matches(".*[,|;].*") && papiIP.matches(".*[,|;].*")) {
            String[] port = papiPort.split("[,;]");
            String[] ip = papiIP.split("[,;]");

            for (int i = 0; i < port.length; i++) {
                purge(ip[i], port[i]);
            }
        } else { // if a single IP/Port couple is specified

            purge(papiIP, papiPort);
        }
    }

    // ////////////////////////////////////// PURGE
    public void purge(String ip, String port) {

        logger.info("PURGE");

        String patternFormat = PAPI_DATE_FORMAT_IN_URI_TREE;

        List<String> patternPathList = new ArrayList<String>();
        List<String> toDeteleList = new ArrayList<String>();
        // Date Formatters used to format purgeStarting date and Now-Date, so
        // they can be used as Integers in comparison
        DateFormat dateFormaterYear = new SimpleDateFormat("yyyy");
        DateFormat dateFormaterMonth = new SimpleDateFormat("yyyyMM");
        DateFormat dateFormaterDay = new SimpleDateFormat("yyyyMMdd");

        PushAPI papiIndexation = null;

        try {
            papiIndexation =
                    PushAPIFactory.createHttp(PushAPIVersion.PAPI_V4, ip, port, connectorName,
                            null, papiLogin, papiPassword);
        } catch (PushAPIException e) {
            logger.error("PAPI ERROR : " + e);
        }
        if (papiIndexation != null) {
            // Added
            Calendar purgeStartingDateCalendar = Calendar.getInstance();
            logger.debug("Time Is : " + purgeStartingDateCalendar.getTime());
            purgeStartingDateCalendar.add(Calendar.DAY_OF_YEAR, -duration);
            logger.debug("Purge Time Is : " + purgeStartingDateCalendar.getTime());
            //

            // /////////// Construct Directory Paths

            // can be modified to eliminate the last path , dayPath, as we have
            // not to go as far as this, this last step have no additional
            // results
            // only when we have to filter by hours, we must go under days
            // enhanced loop should be replaced by a simple loop, end the end
            // point should be modified to ignore the last part of the date
            int i = 0;
            // String tempSplit ="";

            patternPathList.add("/");
            for (String splitFormat : patternFormat.split("/")) {
                String temp = "";
                temp = patternPathList.get(i) + splitFormat + "/";
                patternPathList.add(temp);
                i++;
            }

            // ////////////////////////// SEARCH FOR FOLDERS TO DELETE
            // we can add if clauses for length 10,12,14 to deal with time

            logger.debug("path list is : " + patternPathList);

            // Select RootPath which will be deleted
            i = 0;
            for (String pathPattern : patternPathList) {
                // String folderPath = path;
                logger.debug("################## Searched Path is " + pathPattern);
                DateFormat patternDateFormater = new SimpleDateFormat(pathPattern);
                boolean isFolder = false;
                try {
                    for (SyncedEntry syncedEntry : papiIndexation.enumerateSyncedEntries(
                            patternDateFormater.format(purgeStartingDateCalendar.getTime()),
                            EnumerationMode.NOT_RECURSIVE_FOLDERS)) {

                        // purge is folder oriented, documents with empty params
                        // (year,month,day) (so their root will not be
                        // complete),
                        // will not be affected by the purge job
                        isFolder = syncedEntry.isFolder();
                        logger.debug("uri is  :  " + syncedEntry.getUri());

                        String cleanURI = syncedEntry.getUri().replaceAll("/", "");
                        // if there are results
                        if ((isFolder == true) && (isIntParsable(cleanURI))) {
                            int intCleanURI = Integer.parseInt(cleanURI);
                            logger.debug("uri is folder :  " + syncedEntry.getUri());

                            // D: First Iteration : under root , year folders
                            // will be found
                            // If year
                            if (cleanURI.length() == 4) {
                                logger.debug("uri : " + syncedEntry.getUri() + "is = 4 is "
                                        + (syncedEntry.getUri().replaceAll("/", "").length() == 4));
                                logger.debug("uri after replaceAll is : "
                                        + syncedEntry.getUri().replaceAll("/", ""));
                                logger.debug("comparison between : "
                                        + ","
                                        + Integer
                                                .parseInt(syncedEntry.getUri().replaceAll("/", "")));
                                if (intCleanURI < Integer.parseInt(dateFormaterYear
                                        .format(purgeStartingDateCalendar.getTime()))) {
                                    logger.debug("uri is to be deleted");
                                    toDeteleList.add(syncedEntry.getUri());
                                } else if (intCleanURI > Integer.parseInt(dateFormaterYear
                                        .format(new Date()))) {
                                    toDeteleList.add(syncedEntry.getUri());
                                }

                            }

                            // D: second iteration : folders will have uri
                            // /year/MM
                            // If year Month
                            else if (cleanURI.length() == 6) {
                                if (intCleanURI < Integer.parseInt(dateFormaterMonth
                                        .format(purgeStartingDateCalendar.getTime()))) {
                                    toDeteleList.add(syncedEntry.getUri());
                                } else if (intCleanURI > Integer.parseInt(dateFormaterMonth
                                        .format(new Date()))) {
                                    toDeteleList.add(syncedEntry.getUri());
                                }
                            }

                            // D: Third iteration, Floders will have uri looks
                            // like: /year/MM/dd
                            // If year Month Day
                            else if (cleanURI.length() == 8) {
                                if (intCleanURI < Integer.parseInt(dateFormaterDay
                                        .format(purgeStartingDateCalendar.getTime()))) {
                                    toDeteleList.add(syncedEntry.getUri());
                                } else if (intCleanURI > Integer.parseInt(dateFormaterDay
                                        .format(new Date()))) {
                                    toDeteleList.add(syncedEntry.getUri());
                                }
                            }
                        }
                    }

                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    logger.error(e);
                } catch (PushAPIException e) {
                    // TODO Auto-generated catch block
                    logger.error(e);
                }
            }

            logger.debug("toDeleteList Is : " + toDeteleList);
            // Delete documents with their rootpath
            for (String toDeleteUri : toDeteleList) {
                try {
                    papiIndexation.deleteDocumentRootPath(toDeleteUri);
                } catch (PushAPIException e) {
                    // TODO Auto-generated catch block
                    logger.error(e);
                }
            }

            try {
                papiIndexation.sync();
                papiIndexation.triggerIndexingJob();
            } catch (PushAPIException e) {
                // TODO Auto-generated catch block
                logger.error(e);
            }
        } else {
            logger.error("Purge job  not executed on " + ip + " Port:" + port);
        }
    }

    // ////////////////////////////// Verify if a string can be parsed to an Int
    public boolean isIntParsable(String input) {
        boolean isParsable = true;
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException e) {
            isParsable = false;
        }
        return isParsable;
    }
}
