package com.exalead.job.orchestrator.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

public class MAMIException extends Exception {
	public enum MAMI_EXCEPTION {
		CONNECTOR_NOT_EXISTS, CONNEXION_ERROR, UNKNOWN, TIMEOUT, GENERIC
	};

	private MAMI_EXCEPTION exceptionType = MAMI_EXCEPTION.UNKNOWN;
	private String message = null;
	private Exception parentException = null;

	public MAMIException() {
	}

	public MAMIException(MAMI_EXCEPTION excType, String msg) {
		exceptionType = excType;
		message = msg;
	}

	public MAMIException(MAMI_EXCEPTION excType, Exception parentExc, String msg) {
		exceptionType = excType;
		message = msg;
		parentException = parentExc;
	}

	public MAMIException(Exception parentExc, String msg) {
		message = msg;
		parentException = parentExc;
	}

	@Override
	public String getMessage() {
		return "MAMI exception [" + this.exceptionType + "], " + this.message;
	}

	@Override
	public StackTraceElement[] getStackTrace() {
		return parentException.getStackTrace();
	}

	@Override
	public void printStackTrace() {
		parentException.printStackTrace();
	}

	@Override
	public void printStackTrace(PrintStream s) {
		parentException.printStackTrace(s);
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		parentException.printStackTrace(s);
	}
}
