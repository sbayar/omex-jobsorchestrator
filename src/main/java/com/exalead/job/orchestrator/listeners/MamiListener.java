package com.exalead.job.orchestrator.listeners;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

public class MamiListener implements JobListener {
	private static Log logger = LogFactory.getLog(MamiListener.class);

	@Override
	public String getName() {
		return "MAMI LISTENER";
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext ctx) {
		logger.debug("Job [" + ctx.getJobDetail().getKey().getName() + "] launched");
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext ctx) {
		logger.info("Run job [" + ctx.getJobDetail().getKey().getName() + "]");
	}

	@Override
	public void jobWasExecuted(JobExecutionContext ctx, JobExecutionException exc) {
		logger.info("Job [" + ctx.getJobDetail().getKey().getName() + "] executed");

		if (exc != null)
			logger.error("Job [" + ctx.getJobDetail().getKey().getName() + "] exception [" + exc.getMessage() + "]",
					exc);
	}
}
