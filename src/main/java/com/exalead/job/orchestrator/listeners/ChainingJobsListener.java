package com.exalead.job.orchestrator.listeners;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.SchedulerException;

public class ChainingJobsListener implements JobListener {
	private static Log logger = LogFactory.getLog(ChainingJobsListener.class);

	@Override
	public String getName() {
		return "CHAINING JOBS LISTENER";
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext ctx) {
		logger.debug("Job [" + ctx.getJobDetail().getKey().getName() + "] launched");
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext ctx) {
		logger.info("Run job [" + ctx.getJobDetail().getKey().getName() + "]");
	}

	@Override
	public void jobWasExecuted(JobExecutionContext ctx, JobExecutionException exc) {
		logger.info("Job [" + ctx.getJobDetail().getKey().getName() + "] executed");

		JobKey nextJob = null;
		if (ctx.getJobDetail().getJobDataMap().containsKey("chain")) {
			nextJob = new JobKey(ctx.getJobDetail().getJobDataMap().getString("chain"), ctx.getJobDetail().getKey()
					.getGroup());
		}

		if (exc != null) {
			logger.error("Job [" + ctx.getJobDetail().getKey().getName() + "] exception [" + exc.getMessage() + "]",
					exc);
			if (nextJob != null) {
				logger.info("Will not execute job [" + nextJob + "]");
			}
		} else if (nextJob != null) {
			logger.info("Chain trigger execution of job [" + nextJob + "]");
			try {
				ctx.getScheduler().triggerJob(nextJob);
			} catch (SchedulerException se) {
				logger.error("Error encountered during chaining to Job '" + nextJob + "'", se);
			}
		}
	}
}
