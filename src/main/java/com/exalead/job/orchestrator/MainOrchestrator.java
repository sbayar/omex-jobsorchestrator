package com.exalead.job.orchestrator;

import java.io.File;
import java.net.URL;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

public class MainOrchestrator {

	private final static Log logger = LogFactory.getLog(MainOrchestrator.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainOrchestrator ordo = new MainOrchestrator();
			ordo.run(args);
		} catch (Exception e) {
			logger.error("Error in main orchestrator", e);
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void run(String[] args) throws Exception {
		logger.info("Launch main orchestrator");

		OptionParser opt_parser = new OptionParser();
		OptionSpec<String> configFile = opt_parser.accepts("config", "Scheduler configuration file").withRequiredArg()
				.ofType(String.class);

		final OptionSet options = opt_parser.parse(args);
		File fConfig = null;
		try {
			String filePath = configFile.value(options);
			logger.info("Configuration file path [" + filePath + "]");
			URL fileURL = this.getClass().getClassLoader().getResource(filePath);
			logger.info("Try to load configuration file [" + fileURL + "]");
			fConfig = new File(fileURL.getFile());
		} catch (Exception e) {
			logger.error("Error initializing configuration file", e);
		}

		if (fConfig != null) {
			// First we must get a reference to a scheduler
			StdSchedulerFactory sf = new StdSchedulerFactory();
			sf.initialize(fConfig.getCanonicalPath());
			Scheduler sched = sf.getScheduler();

			// start the scheduler
			sched.start();
		} else {
			throw new Exception("Configuration file not found or not specified");
		}
	}
}
