package com.exalead.job.orchestrator.indexing;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.job.orchestrator.mami.IndexingActionGroup;

public class IndexingThread implements Runnable {
	private final static Log logger = LogFactory.getLog(IndexingThread.class);
	private long interval = 5 * 1000;
	private long timeout = 60 * 60 * 1000;

	private String buildGroup;
	private MAMIClient mamiClient;

	public IndexingThread(String buildGroup, MAMIClient mamiClient) {
		this.buildGroup = buildGroup;
		this.mamiClient = mamiClient;
	}

	public IndexingThread(String buildGroup, MAMIClient mamiClient, long timeout) {
		this.buildGroup = buildGroup;
		this.mamiClient = mamiClient;
		this.timeout = timeout;
	}

	@Override
	public void run() {
		logger.info(Thread.currentThread().getName() + " - Start indexation process BG [" + this.buildGroup + "]");

		Date dStart = new Date();
		long execTime = 0;

		IndexingActionGroup.triggerIndexingJob(mamiClient, buildGroup, true);

		try {
			Thread.sleep(this.interval);
		} catch (InterruptedException e) {
			logger.error("Error in indexing thread", e);
		}

		boolean bIndexing = true;

		while (bIndexing && execTime < this.timeout) {
			Date dCurrent = new Date();
			execTime = dCurrent.getTime() - dStart.getTime();
			bIndexing = IndexingActionGroup.isIndexingOrCompacting(mamiClient, buildGroup);

			try {
				Thread.sleep(this.interval);
			} catch (InterruptedException e) {
				logger.error("Error in compact action", e);
			}
		}

		if (execTime >= this.timeout) {
			logger.warn("Timeout reached executing " + Thread.currentThread().getName() + " --> Indexation BG ["
					+ this.buildGroup + "]");
		}

		logger.debug("Thread " + Thread.currentThread().getName() + " --> Indexation BG [" + this.buildGroup + "] DONE");
	}
}
