package com.exalead.job.orchestrator.indexing;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.job.orchestrator.mami.CompactActionGroup;

public class CompactThread implements Runnable {

	private final static Log logger = LogFactory.getLog(CompactThread.class);
	private long interval = 5 * 1000;
	private long timeout = 60 * 60 * 1000;

	private String buildGroup;
	private MAMIClient mamiClient;
	private int slice;

	public CompactThread(String buildGroup, int slice, MAMIClient mamiClient) {
		this.buildGroup = buildGroup;
		this.slice = slice;
		this.mamiClient = mamiClient;
	}

	public CompactThread(String buildGroup, int slice, MAMIClient mamiClient, long timeout) {
		this.buildGroup = buildGroup;
		this.slice = slice;
		this.mamiClient = mamiClient;
		this.timeout = timeout;
	}

	@Override
	public void run() {
		logger.info(Thread.currentThread().getName() + " - Start full compact BG [" + this.buildGroup + "] slice ["
				+ this.slice + "]");

		Date dStart = new Date();
		long execTime = 0;

		CompactActionGroup.compact(mamiClient, buildGroup, slice);

		boolean bSliceIdle = false;

		while (!bSliceIdle && execTime < this.timeout) {
			Date dCurrent = new Date();
			execTime = dCurrent.getTime() - dStart.getTime();

			bSliceIdle = CompactActionGroup.isBgSliceIdle(mamiClient, buildGroup, slice);

			try {
				Thread.sleep(this.interval);
			} catch (InterruptedException e) {
				logger.error("Error in compact action", e);
			}
		}

		if (execTime >= this.timeout) {
			logger.warn("Timeout reached executing " + Thread.currentThread().getName() + " --> full compact BG ["
					+ this.buildGroup + "] slice [" + this.slice + "]");
		}

		logger.debug("Thread " + Thread.currentThread().getName() + " --> full compact BG [" + this.buildGroup
				+ "] slice [" + this.slice + "] DONE");
	}
}
