package com.exalead.job.orchestrator.connectors;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.mami.ConnectorsActionGroup;
import com.exalead.mercury.mami.connect.v10.ScanStatus;

public class ScanThread implements Runnable {
    private final static Log logger = LogFactory.getLog(ScanThread.class);
    private long interval = 5 * 1000;
    private long timeout = 0;
    private boolean forceScanIfRunning = false;

    private String connectorName;
    private MAMIClient mamiClient;

    public ScanThread(String connectorName, MAMIClient mamiClient, long timeout) {
        this.connectorName = connectorName;
        this.mamiClient = mamiClient;
        this.timeout = timeout;
    }

    public ScanThread(String connectorName, MAMIClient mamiClient, long timeout,
            boolean forceScanIfRunning) {
        this.connectorName = connectorName;
        this.mamiClient = mamiClient;
        this.timeout = timeout;
        this.forceScanIfRunning = forceScanIfRunning;
    }

    @Override
    public void run() {
        try {
            scan();
        } catch (Exception e) {
            logger.error(
                    "Error executing " + Thread.currentThread().getName() + " --> "
                            + e.getMessage(), e);
        }
    }

    private void scan() throws InterruptedException {
        boolean connectorIdle = false;

        try {
            connectorIdle = ConnectorsActionGroup.isConnectorIdle(connectorName, mamiClient);
        } catch (MAMIException e) {
            logger.warn("Error getting connector [" + connectorName
                    + "] status (is idle) --> considering not idle...", e);
        }

        if (connectorIdle || forceScanIfRunning) {
            logger.info(Thread.currentThread().getName() + " - Start full rescan ["
                    + this.connectorName + "]");
            ConnectorsActionGroup.forceConnectorFullRescan(connectorName, mamiClient);

            Date dStart = new Date();
            long execTime = 0;

            // Wait for full scan taken into account
            Thread.sleep(this.interval);

            try {
                connectorIdle = ConnectorsActionGroup.isConnectorIdle(connectorName, mamiClient);
            } catch (MAMIException e) {
                logger.warn("Error getting connector [" + connectorName
                        + "] status (is idle) --> considering not idle...", e);
            }

            while (!connectorIdle && execTime < this.timeout) {
                Date dCurrent = new Date();
                execTime = dCurrent.getTime() - dStart.getTime();

                long docCount = -1;

                try {
                    docCount =
                            ConnectorsActionGroup.getConnectorPushedDocuments(this.connectorName,
                                    this.mamiClient);
                } catch (Exception e) {
                    logger.warn("Error getting connector [" + connectorName
                            + "] number of pushed documents", e);
                }
                logger.info("Thread " + Thread.currentThread().getName()
                        + " --> full scan connector [" + connectorName + "] --> pushed documents ["
                        + docCount + "]");

                Thread.sleep(this.interval);

                try {
                    connectorIdle =
                            ConnectorsActionGroup.isConnectorIdle(connectorName, mamiClient);
                } catch (MAMIException e) {
                    logger.warn("Error getting connector [" + connectorName
                            + "] status (is idle) --> considering not idle...", e);
                }
            }

            if (connectorIdle) {
                try {
                    ScanStatus scanStatus =
                            ConnectorsActionGroup.getConnectorPreviousScanStatus(connectorName,
                                    mamiClient);
                    if (scanStatus != null && scanStatus.isAborted()) {
                        logger.warn("Connector [" + connectorName + "] scan aborted (Exception ["
                                + scanStatus.getException() + "])");
                    }
                } catch (MAMIException e) {
                    logger.warn("Error getting connector Status for connector [" + connectorName
                            + "]", e);
                }
            }
            if (execTime >= this.timeout) {
                logger.warn("Timeout reached executing " + Thread.currentThread().getName()
                        + " --> full scan connector [" + connectorName + "] (" + execTime + ")");
            }

            logger.info("Thread " + Thread.currentThread().getName() + " --> full scan connector ["
                    + connectorName + "] DONE (" + execTime + ")");
        } else {
            logger.warn("Connector [" + connectorName + "] not idle --> will not execute full scan");
        }
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }
}
