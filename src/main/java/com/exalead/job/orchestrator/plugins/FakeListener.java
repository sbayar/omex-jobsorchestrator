package com.exalead.job.orchestrator.plugins;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.ClassLoadHelper;
import org.quartz.spi.SchedulerPlugin;

public class FakeListener implements SchedulerPlugin, JobListener {
	private static final Log logger = LogFactory.getLog(FakeListener.class);

	@Override
	public String getName() {
		return "Fake listener";
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext paramJobExecutionContext) {
		logger.info("JOB EXECUTION VETOED (Fake listener)");
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext paramJobExecutionContext) {
		logger.info("JOB TO BE EXECUTED (Fake listener)");
	}

	@Override
	public void jobWasExecuted(JobExecutionContext paramJobExecutionContext,
			JobExecutionException paramJobExecutionException) {
		logger.info("JOB WAS EXECUTED (Fake listener)");
	}

	@Override
	public void initialize(String paramString, Scheduler paramScheduler, ClassLoadHelper paramClassLoadHelper)
			throws SchedulerException {
		logger.info("INITIALIZED FAKE LISTENER");
	}

	@Override
	public void shutdown() {
		logger.info("SHUTDOWN FAKE LISTENER");
	}

	@Override
	public void start() {
		logger.info("START FAKE LISTENER");
	}

}
