package com.exalead.job.orchestrator.plugins;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.matchers.EverythingMatcher;
import org.quartz.spi.ClassLoadHelper;
import org.quartz.spi.SchedulerPlugin;

public class ChainingJobsPlugin implements SchedulerPlugin, JobListener {
	private static final Log logger = LogFactory.getLog(ChainingJobsPlugin.class);

	private String chainKey;
	private String name = "Chain jobs listener plugin";

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext ctx) {
		logger.info("Run job [" + ctx.getJobDetail().getKey().getName() + "]");
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext ctx) {
		logger.debug("Job [" + ctx.getJobDetail().getKey().getName() + "] launched");
	}

	@Override
	public void jobWasExecuted(JobExecutionContext ctx, JobExecutionException exc) {
		logger.info("Job [" + ctx.getJobDetail().getKey().getName() + "] executed");

		JobKey nextJob = null;
		if (ctx.getJobDetail().getJobDataMap().containsKey(chainKey)) {
			nextJob = new JobKey(ctx.getJobDetail().getJobDataMap().getString(chainKey), ctx.getJobDetail().getKey()
					.getGroup());
		}

		if (exc != null) {
			logger.error("Job [" + ctx.getJobDetail().getKey().getName() + "] exception [" + exc.getMessage() + "]",
					exc);
			if (nextJob != null) {
				logger.info("Will not execute job [" + nextJob + "]");
			}
		} else if (nextJob != null) {
			logger.info("Chain trigger execution of job [" + nextJob + "]");
			try {
				ctx.getScheduler().triggerJob(nextJob);
			} catch (SchedulerException se) {
				logger.error("Error encountered during chaining to Job '" + nextJob + "'", se);
			}
		}
	}

	@Override
	public void initialize(String pname, Scheduler scheduler, ClassLoadHelper paramClassLoadHelper)
			throws SchedulerException {
		if (!StringUtils.isEmpty(pname)) {
			this.name = pname;
		}
		scheduler.getListenerManager().addJobListener(this, EverythingMatcher.allJobs());
		logger.info("Chain jobs listener initialized");
	}

	@Override
	public void start() {
		logger.info("Chain jobs listener started");
	}

	@Override
	public void shutdown() {
		logger.info("Chain jobs listener shutdown");
	}

	public String getChainKey() {
		return chainKey;
	}

	public void setChainKey(String chainKey) {
		this.chainKey = chainKey;
	}
}
