package com.exalead.job.orchestrator.mami;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.exception.MAMIException.MAMI_EXCEPTION;

import exa.bee.deploy.v10.DeploymentStatus;
import exa.bee.deploy.v10.GetDeploymentStatus;
import exa.bee.deploy.v10.HostStatus;
import exa.bee.deploy.v10.ProcessStatus;
import exa.bee.deploy.v10.StartProcess;
import exa.bee.deploy.v10.StopProcess;

public class ProcessActionGroup {
	private final static Log logger = LogFactory.getLog(ProcessActionGroup.class);

	public static void stopProcess(String hostname, String instance, String process, MAMIClient client) {
		StopProcess stopProcess = new StopProcess();
		stopProcess.setHostname(hostname);
		stopProcess.setInstallName(instance);
		stopProcess.setProcessName(process);
		logger.debug("Stop process [" + process + "] on host [" + hostname + "] install [" + instance + "]");

		client.getMAMIManagers().getDeploymentManager().stopProcess(stopProcess);
	}

	public static void startProcess(String hostname, String instance, String process, MAMIClient client) {
		StartProcess startProcess = new StartProcess();
		startProcess.setHostname(hostname);
		startProcess.setInstallName(instance);
		startProcess.setProcessName(process);
		logger.debug("Start process [" + process + "] on host [" + hostname + "] install [" + instance + "]");

		client.getMAMIManagers().getDeploymentManager().startProcess(startProcess);
	}

	public static String getProcessStatus(String hostname, String instance, String process, MAMIClient client) {
		GetDeploymentStatus getDeploymentStatus = new GetDeploymentStatus();
		DeploymentStatus deploymentStatus = client.getMAMIManagers().getDeploymentManager()
				.getDeploymentStatus(getDeploymentStatus);

		for (HostStatus hostStatus : deploymentStatus.getHostStatus()) {
			if (hostStatus.getHostname().equals(hostname) && hostStatus.getInstall().equals(instance)) {
				for (ProcessStatus ps : hostStatus.getProcessStatus()) {
					if (ps.getProcessName().equals(process)) {
						return ps.getStatus();
					}
				}
			}
		}

		return null;
	}

	public static void restartProcess(String hostname, String instance, String process, MAMIClient client, long timeout)
			throws MAMIException {
		boolean processStropped = false;
		boolean processStarted = false;

		boolean stopExecuted = false;
		boolean startExecuted = false;

		while (!stopExecuted) {
			try {
				stopProcess(hostname, instance, process, client);
				stopExecuted = true;
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					logger.error("Error sleep", e);
				}
			} catch (Exception e) {
				logger.warn("Error executing stop process command for process [" + process + "] --> retry in 5s", e);
			}
		}

		long startStamp = System.currentTimeMillis();
		long duration = 0;

		while (duration < timeout && !processStropped) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				logger.error("Error sleep", e);
			}
			duration = System.currentTimeMillis() - startStamp;
			try {
				processStropped = "stopped".equals(getProcessStatus(hostname, instance, process, client));
			} catch (Exception e) {
				logger.warn("Error getting process status for process [" + process + "] --> retry in 5s", e);
			}
		}

		if (duration >= timeout) {
			throw new MAMIException(MAMI_EXCEPTION.TIMEOUT, "Timeout reached while stopping process [" + process
					+ "] on host [" + hostname + "] install [" + instance + "]");
		}

		while (!startExecuted) {
			try {
				startProcess(hostname, instance, process, client);
				startExecuted = true;
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					logger.error("Error sleep", e);
				}
			} catch (Exception e) {
				logger.warn("Error executing start process command for process [" + process + "] --> retry in 5s", e);
			}
		}

		startStamp = System.currentTimeMillis();
		duration = 0;

		while (duration < timeout && !processStarted) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				logger.error("Error sleep", e);
			}
			duration = System.currentTimeMillis() - startStamp;

			try {
				processStarted = "started".equals(getProcessStatus(hostname, instance, process, client));
			} catch (Exception e) {
				logger.warn("Error getting process status for process [" + process + "] --> retry in 5s", e);
			}
		}

		if (duration >= timeout) {
			throw new MAMIException(MAMI_EXCEPTION.TIMEOUT, "Timeout reached while starting process [" + process
					+ "] on host [" + hostname + "] install [" + instance + "]");
		}
	}
}
