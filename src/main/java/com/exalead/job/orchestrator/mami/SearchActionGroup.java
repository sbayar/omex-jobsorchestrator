package com.exalead.job.orchestrator.mami;

import java.util.ArrayList;
import java.util.List;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.mercury.mami.search.v20.BuildSuggest;
import com.exalead.mercury.mami.search.v20.GetSuggestStatusList;
import com.exalead.mercury.mami.search.v20.Suggest;
import com.exalead.mercury.mami.search.v20.SuggestStatus;
import com.exalead.mercury.mami.search.v20.SuggestStatusList;

public class SearchActionGroup {

	public static List<String> getSuggestsList(MAMIClient client, String[] regexps) {
		List<String> suggests = new ArrayList<String>();

		if (client.getMAMIManagers().getSearchManager().getSuggestConfig().getSuggest() != null) {
			for (Suggest suggest : client.getMAMIManagers().getSearchManager().getSuggestConfig().getSuggest()) {
				for (String regexp : regexps) {
					if (suggest.getSuggestName().matches(regexp) && !suggests.contains(suggest.getSuggestName()))
						suggests.add(suggest.getSuggestName());
				}

			}
		}

		return suggests;
	}

	public static void buildSuggest(MAMIClient client, String suggestName) {
		BuildSuggest bs = new BuildSuggest();
		bs.setSuggestName(suggestName);

		client.getMAMIManagers().getSearchManager().buildSuggest(bs);
	}

	public static boolean areSuggestIdle(MAMIClient client, List<String> suggestNames) {
		GetSuggestStatusList getStatus = new GetSuggestStatusList();
		SuggestStatusList list = client.getMAMIManagers().getSearchManager().getSuggestStatusList(getStatus);
		if (list != null) {
			for (SuggestStatus status : list.getSuggestStatus()) {
				if (!"idle".equalsIgnoreCase(status.getStatus()) && suggestNames.contains(status.getSuggestName()))
					return false;
			}
		}

		return true;
	}

}
