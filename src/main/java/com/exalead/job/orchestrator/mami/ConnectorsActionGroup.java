package com.exalead.job.orchestrator.mami;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.job.orchestrator.exception.MAMIException;
import com.exalead.job.orchestrator.exception.MAMIException.MAMI_EXCEPTION;
import com.exalead.mercury.mami.connect.v10.ClearConnector;
import com.exalead.mercury.mami.connect.v10.Connector;
import com.exalead.mercury.mami.connect.v10.ConnectorList;
import com.exalead.mercury.mami.connect.v10.ConnectorStatus;
import com.exalead.mercury.mami.connect.v10.ForceConnectorFullRescan;
import com.exalead.mercury.mami.connect.v10.PerBuildGroupStatus;
import com.exalead.mercury.mami.connect.v10.ScanStatus;

public class ConnectorsActionGroup {

	public static void forceConnectorFullRescan(String connectorName, MAMIClient client) {
		ForceConnectorFullRescan conReScan = new ForceConnectorFullRescan();
		conReScan.setConnectorName(connectorName);
		client.getMAMIManagers().getConnectManager().forceConnectorFullRescan(conReScan);
	}

	public static void clearConnector(String connectorName, MAMIClient client) {
		ClearConnector clearCon = new ClearConnector();
		clearCon.setConnectorName(connectorName);
		client.getMAMIManagers().getConnectManager().clearConnector(clearCon);
	}

	public static boolean isConnectorIdle(String connectorName, MAMIClient client) throws MAMIException {
		ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
				.getConnectorStatusByConnectorName(connectorName);

		if (status == null) {
			throw new MAMIException(MAMI_EXCEPTION.CONNECTOR_NOT_EXISTS, "Connector status for connector ["
					+ connectorName + "] not found");
		}

		return status.getStatus().equalsIgnoreCase("idle");
	}

	public static boolean areConnectorsIdle(MAMIClient client, String processName) throws MAMIException {
		List<ConnectorStatus> allStatus = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
				.getConnectorStatus();

		for (ConnectorStatus status : allStatus) {
			if (("connectors-" + status.connectorServer).equals(processName)
					&& !"idle".equalsIgnoreCase(status.getStatus())) {
				return false;
			}
		}

		return true;
	}

	public static boolean areConnectorsIdle(MAMIClient client, String... connectorNames) throws MAMIException {
		for (String connectorName : connectorNames) {
			ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
					.getConnectorStatusByConnectorName(connectorName);

			if (status == null) {
				throw new MAMIException(MAMI_EXCEPTION.CONNECTOR_NOT_EXISTS, "Connector status for connector ["
						+ connectorName + "] not found");
			}

			if (!status.getStatus().equalsIgnoreCase("idle")) {
				return false;
			}
		}

		return true;
	}
	
	
	public static boolean areConnectorsWorking(MAMIClient client, String... connectorNames) throws MAMIException {
      for (String connectorName : connectorNames) {
          ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
                  .getConnectorStatusByConnectorName(connectorName);

          if (status == null) {
              throw new MAMIException(MAMI_EXCEPTION.CONNECTOR_NOT_EXISTS, "Connector status for connector ["
                      + connectorName + "] not found");
          }

          if (status.getStatus().equalsIgnoreCase("working")) {
              return true;
          }
      }

      return false;
  }
	
	

	public static ScanStatus getConnectorPreviousScanStatus(String connectorName, MAMIClient client)
			throws MAMIException {
		ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
				.getConnectorStatusByConnectorName(connectorName);

		if (status == null) {
			throw new MAMIException(MAMI_EXCEPTION.CONNECTOR_NOT_EXISTS, "Connector status for connector ["
					+ connectorName + "] not found");
		}

		if (status.getPreviousScan() != null) {
			return status.getPreviousScan().getScanStatus();
		}

		return null;
	}

	public static List<String> getConnectorsList(MAMIClient client, String[] regexps) {
		List<String> connectors = new ArrayList<String>();
		ConnectorList connectorList = client.getMAMIManagers().getConnectManager().getConnectorList();
		for (String regexp : regexps) {
			for (Connector connector : connectorList.getConnector()) {
				if (connector.getName().matches(regexp) && !connectors.contains(connector.getName())) {
					connectors.add(connector.getName());
				}
			}
		}
		return connectors;
	}

	public static List<String> getConnectorsBGs(MAMIClient client, String[] regexps) {
		List<String> bgs = new ArrayList<String>();
		for (Connector connector : client.getMAMIManagers().getConnectManager().getConnectorList().getConnector()) {
			for (String regexp : regexps) {
				if (connector.getName().matches(regexp) && !bgs.contains(connector.getBuildGroup())) {
					bgs.add(connector.getBuildGroup());
				}
			}
		}
		return bgs;
	}

	public static boolean existsConnector(String connectorName, MAMIClient client) throws MAMIException {
		ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
				.getConnectorStatusByConnectorName(connectorName);

		return status != null;
	}

	public static Map<String, PerBuildGroupStatus> getConnectorBGs(String connectorName, MAMIClient client) throws MAMIException {

		ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
				.getConnectorStatusByConnectorName(connectorName);

		if (status == null) {
			throw new MAMIException(MAMI_EXCEPTION.CONNECTOR_NOT_EXISTS, "Connector status for connector ["
					+ connectorName + "] not found");
		}

		Map<String, PerBuildGroupStatus> bgStatus = status.getPerBuildGroupStatusAsMap();
		return bgStatus;
	}
	
	public static long getConnectorPushedDocuments(String connectorName, MAMIClient client) throws MAMIException {

		ConnectorStatus status = client.getMAMIManagers().getConnectManager().getConnectorsStatus()
				.getConnectorStatusByConnectorName(connectorName);

		if (status == null) {
			throw new MAMIException(MAMI_EXCEPTION.CONNECTOR_NOT_EXISTS, "Connector status for connector ["
					+ connectorName + "] not found");
		}

		List<PerBuildGroupStatus> bgStatus = status.getPerBuildGroupStatus();
		if (bgStatus != null && bgStatus.size() > 0) {
			return bgStatus.get(0).activeDocumentsCount;
		}

		return 0;
	}

	public static boolean isConnectorsEmpty(List<String> connectorNames, MAMIClient client) throws MAMIException {

		for (String connectorName : connectorNames) {
			if (getConnectorPushedDocuments(connectorName, client) > 0) {
				return false;
			}
		}

		return true;
	}
}
