package com.exalead.job.orchestrator.mami;

import java.util.ArrayList;
import java.util.List;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.dictionary.v10.CompactAndBuildDictionary;
import com.exalead.dictionary.v10.DictionaryConfig;
import com.exalead.dictionary.v10.GetDictionaryResourcesInfo;

public class DictionaryActionGroup {

    public static List<String> getDictionaryList(MAMIClient client, String[] regexps) {
        List<String> dictionaries = new ArrayList<String>();
        if (client.getMAMIManagers().getLinguisticManager().getDictionaryConfigList()
                .getDictionaryConfig() != null) {
            for (DictionaryConfig dictionary : client.getMAMIManagers().getLinguisticManager()
                    .getDictionaryConfigList().getDictionaryConfig()) {
                for (String regexp : regexps) {
                    if (dictionary.getName().matches(regexp)
                            && !dictionaries.contains(dictionary.getName()))
                        dictionaries.add(dictionary.getName());
                }

            }
        }
        return dictionaries;
    }

    public static void compactAndBuildDictionary(MAMIClient client, String dictionaryName) {
        CompactAndBuildDictionary cbd = new CompactAndBuildDictionary();
        cbd.setDictionaryName(dictionaryName);
        client.getMAMIManagers().getLinguisticManager().compactAndBuildDictionary(cbd);
    }

    public static void compactAndBuild(MAMIClient client) {
        if (client.getMAMIManagers().getLinguisticManager().getDictionaryConfigList()
                .getDictionaryConfig() != null) {
            for (DictionaryConfig dictionary : client.getMAMIManagers().getLinguisticManager()
                    .getDictionaryConfigList().getDictionaryConfig()) {
                compactAndBuildDictionary(client, dictionary.getName());
            }
        }
    }

    public static boolean areDictionaryIdle(MAMIClient client, List<String> dictionaryNames) {
        for (String name : dictionaryNames) {
            GetDictionaryResourcesInfo g = new GetDictionaryResourcesInfo();
            g.setDictionaryName(name);
            client.getMAMIManagers().getLinguisticManager().getDictionaryResourcesInfo(g);
            if (client.getMAMIManagers().getLinguisticManager().getDictionaryResourcesInfo(g)
                    .isBuilding()
                    && client.getMAMIManagers().getLinguisticManager()
                            .getDictionaryResourcesInfo(g).isCompacting()) {
                return false;
            }
        }
        return true;
    }

}
