package com.exalead.job.orchestrator.mami;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.mercury.mami.deploy.v10.RestartHost;
import com.exalead.mercury.mami.indexing.v10.BuildGroupStatus;
import com.exalead.mercury.mami.indexing.v10.DisableAnalysis;
import com.exalead.mercury.mami.indexing.v10.FullCompactIndex;
import com.exalead.mercury.mami.indexing.v10.GetBuildGroupStatus;
import com.exalead.mercury.mami.indexing.v10.IndexSliceBuilderStatus;

import exa.bee.deploy.v10.DeploymentConfig;
import exa.bee.deploy.v10.Host;
import exa.bee.deploy.v10.Role;
import exa.bee.deploy.v10.RoleAttribute;

public class CompactActionGroup {

	private final static Log TRACEUR = LogFactory.getLog(CompactActionGroup.class);

	private enum Mode {
		COMPACT, CHECKCOMPACTED
	};

	/**
	 * ex: "http://ngdev033.paris.exalead.com:11011" "compact" ex:
	 * "http://ngdev033.paris.exalead.com:11011" "checkcompacted" "bg0"
	 * 
	 * Will go through all slices of the bg is bg specified Will go through all
	 * bg if not specified
	 * 
	 * When runned with mode checkcompacted: - if all slices are idle (not
	 * compacting or importing) => exit with code 1 - if one slice is compacting
	 * or importing => exit with code 2
	 */
	public static void main(String[] args) {
		String gatewayUrl = null;
		Mode mode = null;
		String targetedBg = null;
		Map<String, List<Integer>> bgSlices = null;
		MAMIClient client = null;

		TRACEUR.info("Starting with params: " + Arrays.toString(args));

		if (args == null || args.length < 2) {
			TRACEUR.error("Missing params");
			return;
		}

		gatewayUrl = args[0];

		if ("compact".equalsIgnoreCase(args[1])) {
			mode = Mode.COMPACT;
		} else if ("checkcompacted".equalsIgnoreCase(args[1])) {
			mode = Mode.CHECKCOMPACTED;
		} else {
			TRACEUR.error("Unsupported mode: " + args[1]);
			return;
		}

		if (args.length > 2) {
			targetedBg = args[2];
		}

		client = MAMIClientFactory.build(gatewayUrl);

		bgSlices = gatherSlicesTargets(client);

		if (targetedBg != null && !bgSlices.containsKey(targetedBg)) {
			TRACEUR.error("Targeted bg: " + targetedBg + " doesn't exists in deployment configuration: " + bgSlices);
			System.exit(-1);
		}

		switch (mode) {
		case COMPACT:

			if (targetedBg != null) {
				for (Integer sliceNum : bgSlices.get(targetedBg)) {
					compact(client, targetedBg, sliceNum);
				}
			} else {
				for (Entry<String, List<Integer>> entry : bgSlices.entrySet()) {
					for (Integer sliceNum : entry.getValue()) {
						compact(client, entry.getKey(), sliceNum);
					}
				}
			}

			break;

		case CHECKCOMPACTED:

			Boolean isIdle = null;

			if (targetedBg != null) {
				isIdle = areBgSlicesIdles(client, targetedBg);
			} else {
				for (String bgName : bgSlices.keySet()) {
					boolean tmpIsIdle = areBgSlicesIdles(client, bgName);

					if (isIdle == null) {
						isIdle = tmpIsIdle;
					} else {
						isIdle = isIdle && tmpIsIdle;
					}
				}
			}

			if (isIdle == null) {
				TRACEUR.error("Abnormal state");
				System.exit(-1);
			}

			if (isIdle) {
				System.exit(1);
			} else {
				System.exit(2);
			}

			break;

		default:
			TRACEUR.error("Unsupported mode: " + mode);
			System.exit(-1);
			break;
		}
	}

	public static void disableAnalysis(MAMIClient client, String bg) {
		DisableAnalysis disableAnalysis = new DisableAnalysis();
		disableAnalysis.setBuildGroup(bg);
		disableAnalysis.setPersistent(true);

		client.getManagers().getIndexingManager().disableAnalysis(disableAnalysis);
	}

	public static void compact(MAMIClient client, String bg, int sliceNum) {
		FullCompactIndex compactSlice = new FullCompactIndex();
		compactSlice.setBuildGroup(bg);
		compactSlice.setIndexSlices(String.valueOf(sliceNum));

		client.getManagers().getIndexingManager().fullCompactIndex(compactSlice);

		TRACEUR.info("Compact request sent for bg: [" + bg + "] slice: [" + sliceNum + "]");

	}

	public static boolean isAnalysisEnabled(MAMIClient client, String bg) {
		GetBuildGroupStatus gbs = new GetBuildGroupStatus();
		gbs.setBuildGroup(bg);

		BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

		return bgs.getIndexingStatus().getAnalysisStatus().isEnabled();
	}

	public static void restartHost(MAMIClient client, String hostName, String instanceName) {
		RestartHost restartHost = new RestartHost();
		restartHost.setHostname(hostName);
		restartHost.setInstall(instanceName);

		client.getManagers().getDeploymentManager().restartHost(restartHost);
	}

	public static boolean isBgSliceIdle(MAMIClient client, String bg, int slice) {
		GetBuildGroupStatus gbs = new GetBuildGroupStatus();
		gbs.setBuildGroup(bg);

		BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

		for (IndexSliceBuilderStatus sliceStatus : bgs.getIndexBuilderStatus().getIndexSliceBuilderStatus()) {
			if (sliceStatus.getIndexSlice() == slice) {
				return !sliceStatus.isCompacting() && !sliceStatus.isImporting();
			}
		}

		return true;
	}

	public static boolean areBgSlicesIdles(MAMIClient client, String bg) {
		GetBuildGroupStatus gbs = new GetBuildGroupStatus();
		gbs.setBuildGroup(bg);
		boolean isIdle = true;

		BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

		for (IndexSliceBuilderStatus sliceStatus : bgs.getIndexBuilderStatus().getIndexSliceBuilderStatus()) {
			if (sliceStatus.isCompacting() || sliceStatus.isImporting()) {
				isIdle = isIdle & false;

				TRACEUR.info("Slice: [" + sliceStatus.getIndexSlice() + "] bg: [" + bg + "] compacting: ["
						+ sliceStatus.isCompacting() + "] importing: [" + sliceStatus.isImporting() + "]");
			} else {
				isIdle = isIdle & true;
			}
		}

		return isIdle;
	}

	/**
	 * Hard work to identify attachable / detachable slices WTF ???!!!!
	 * 
	 * @param client
	 */
	public static Map<String, List<Integer>> gatherSlicesTargets(MAMIClient client) {
		DeploymentConfig conf = null;

		Map<String, List<Integer>> bgSlices = null;

		conf = client.getManagers().getDeploymentManager().getDeploymentConfig();

		bgSlices = new HashMap<String, List<Integer>>();

		for (Host host : conf.getHost()) {
			for (Role role : host.getRole()) {
				if ("Indexing".equalsIgnoreCase(role.getName())) {
					// Getroleattrbyname a failli march� ...
					// String bgname =
					// role.getRoleAttributeByName("buildGroup").getValue();

					String bgname = null;
					int nbSlice = 0;

					for (RoleAttribute attr : role.getRoleAttribute()) {
						if ("buildGroup".equalsIgnoreCase(attr.getName())) {
							bgname = attr.getValue();
						} else if ("nbSlices".equalsIgnoreCase(attr.getName())) {
							nbSlice = Integer.valueOf(attr.getValue());
						}
					}

					List<Integer> sliceNums = new ArrayList<Integer>();

					for (int i = 0; i < nbSlice; i++) {
						sliceNums.add(i);
					}

					bgSlices.put(bgname, sliceNums);
				}
			}
		}

		return bgSlices;
	}
}
