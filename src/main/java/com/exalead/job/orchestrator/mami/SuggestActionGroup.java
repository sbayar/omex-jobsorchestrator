package com.exalead.job.orchestrator.mami;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.mercury.mami.search.v20.BuildSuggest;
import com.exalead.mercury.mami.search.v20.Suggest;

public class SuggestActionGroup {

	private final static Log logger = LogFactory.getLog(SuggestActionGroup.class);

	public static void buildSuggest(String suggestName, MAMIClient client) {
		BuildSuggest buildSuggest = new BuildSuggest(suggestName);
		logger.debug("Build suggest [" + suggestName + "]");
		client.getMAMIManagers().getSearchManager().buildSuggest(buildSuggest);
	}

	public static void buildSuggests(MAMIClient client) {
		if (client.getMAMIManagers().getSearchManager().getSuggestConfig() != null
				&& client.getMAMIManagers().getSearchManager().getSuggestConfig().getSuggest() != null) {
			for (Suggest suggest : client.getMAMIManagers().getSearchManager().getSuggestConfig().getSuggest()) {
				buildSuggest(suggest.getSuggestName(), client);
			}
		}
	}
}
