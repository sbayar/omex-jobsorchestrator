package com.exalead.job.orchestrator.mami;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.job.orchestrator.jobs.ConnectorsFullScan;
import com.exalead.mercury.mami.indexing.v10.AreDocumentsSearchable;
import com.exalead.mercury.mami.indexing.v10.BuildGroupStatus;
import com.exalead.mercury.mami.indexing.v10.ClearBuildGroup;
import com.exalead.mercury.mami.indexing.v10.GetBuildGroupStatus;
import com.exalead.mercury.mami.indexing.v10.IndexSliceBuilderStatus;
import com.exalead.mercury.mami.indexing.v10.SyncTaskQueue;
import com.exalead.mercury.mami.indexing.v10.TriggerIndexingJob;

import exa.bee.StringConstantValue;

public class IndexingActionGroup {
	private static Log logger = LogFactory.getLog(IndexingActionGroup.class);
	
	public static boolean isIndexingRunning(MAMIClient client, String bg) {
		GetBuildGroupStatus gbs = new GetBuildGroupStatus();
		gbs.setBuildGroup(bg);

		BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

		return bgs.getIndexingStatus().getAnalysisStatus().isProcessing()
				|| bgs.getIndexingStatus().getImportStatus().isProcessing();
	}

	public static boolean isIndexingRunning(MAMIClient client, List<String> bgList) {
		for (String bg : bgList) {
			if (isIndexingRunning(client, bg))
				return true;
		}

		return false;
	}

	public static void triggerIndexingJob(MAMIClient client, String bg, boolean syncTQ) {
		if (syncTQ) {
			// Sync TQ needed in 14+ version to commit in case of TQLess BG
			SyncTaskQueue sync = new SyncTaskQueue();
			sync.setBuildGroup(bg);
			client.getManagers().getIndexingManager().syncTaskQueue(sync);
		}

		TriggerIndexingJob triggerBG = new TriggerIndexingJob();
		triggerBG.setBuildGroup(bg);

		client.getManagers().getIndexingManager().triggerIndexingJob(triggerBG);
	}

	public static void triggerIndexingJob(MAMIClient client, String bg) {
		triggerIndexingJob(client, bg, true);
	}

	public static boolean isIndexingOrCompacting(MAMIClient client, String bg) {
		return isIndexingRunning(client, bg) || isCompactingBG(bg, client);
	}

	public static boolean isIndexingOrCompacting(MAMIClient client, List<String> bgList) {
		return isIndexingRunning(client, bgList) || isCompactingBGs(bgList, client);
	}

	public static boolean isCompactingBGs(List<String> bgList, MAMIClient client) {
		for (String bgName : bgList) {
			if (isCompactingBG(bgName, client))
				return true;
		}

		return false;
	}

	public static boolean isCompactingBG(String bgName, MAMIClient client) {
		GetBuildGroupStatus gbs = new GetBuildGroupStatus();
		gbs.setBuildGroup(bgName);

		BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

		for (IndexSliceBuilderStatus isbs : bgs.getIndexBuilderStatus().getIndexSliceBuilderStatus()) {
			if (isbs.isCompacting())
				return true;
		}

		return false;
	}

	public static void clearMasterBuildGroup(String bgName, MAMIClient client) {
		clearBuildGroup(bgName, client, true);
	}

	public static void clearBuildGroup(String bgName, MAMIClient client, boolean keepSlave) {
		ClearBuildGroup clearBg = new ClearBuildGroup();
		clearBg.setBuildGroup(bgName);
		clearBg.setKeepReplicatedIndexInstances(keepSlave);

		client.getManagers().getIndexingManager().clearBuildGroup(clearBg);
	}

	public static boolean isBGMasterEmpty(String bgName, MAMIClient client) {
		GetBuildGroupStatus gbs = new GetBuildGroupStatus();
		gbs.setBuildGroup(bgName);

		BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

		for (IndexSliceBuilderStatus isbs : bgs.getIndexBuilderStatus().getIndexSliceBuilderStatus()) {
			if (isbs.getNdocs() > 0)
				return false;
		}

		return true;
	}

	public static boolean isBGMasterEmpty(List<String> bgList, MAMIClient client) {
		for (String bgName : bgList) {
			if (!isBGMasterEmpty(bgName, client))
				return false;
		}

		return true;
	}
	
	public static boolean areDocumentsSearchable(MAMIClient client, long serial) {
		AreDocumentsSearchable ads = new AreDocumentsSearchable();
		ads.setSerial(serial);
		ads.setBuildGroup("bg0");
		String isSearchable = client.getManagers().getIndexingManager().areDocumentsSearchable(ads).getValue();
		logger.info("serial:" + serial+ " is searchable ? " +  isSearchable );
		return isSearchable.equalsIgnoreCase("true");
	}
	
	public static void unfreeze(MAMIClient client){
	  client.getManagers().getMasterManager().unfreeze();
	}

}
