package com.exalead.job.orchestrator.mami;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.exalead.cloudview.mami.client.MAMIClient;
import com.exalead.cloudview.mami.client.MAMIClientFactory;
import com.exalead.mercury.mami.indexing.v10.AttachSliceReplica;
import com.exalead.mercury.mami.indexing.v10.BuildGroupStatus;
import com.exalead.mercury.mami.indexing.v10.ClearSliceReplica;
import com.exalead.mercury.mami.indexing.v10.DetachSliceReplica;
import com.exalead.mercury.mami.indexing.v10.GetBuildGroupStatus;
import com.exalead.mercury.mami.indexing.v10.IndexSliceInstanceStatus;

import exa.bee.deploy.v10.DeploymentConfig;
import exa.bee.deploy.v10.Host;
import exa.bee.deploy.v10.Role;
import exa.bee.deploy.v10.RoleAttribute;

public class ReplicaActionGroup {

	private final static Log logger = LogFactory.getLog(ReplicaActionGroup.class);

	public enum Mode {
		ATTACH, DETACH, CLEARREPLICA
	};

	private final static long SLEEPTIME = 2000;

	/**
	 * ex: "http://ngdev033.paris.exalead.com:11011" "detach" ex:
	 * "http://ngdev033.paris.exalead.com:11011" "attach" "bg0"
	 * 
	 * Will go through all slices of the bg is bg specified Will go through all
	 * bg if not specified
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String gatewayUrl = null;
		Mode mode = null;
		String targetedBg = null;
		Map<String, Map<String, List<Integer>>> bgInstanceSlice = null;
		MAMIClient client = null;

		logger.info("Starting with params: " + Arrays.toString(args));

		if (args == null || args.length < 2) {
			logger.error("Missing params");
			return;
		}

		gatewayUrl = args[0];

		if ("attach".equalsIgnoreCase(args[1])) {
			mode = Mode.ATTACH;
		} else if ("detach".equalsIgnoreCase(args[1])) {
			mode = Mode.DETACH;
		} else if ("clearreplica".equalsIgnoreCase(args[1])) {
			mode = Mode.CLEARREPLICA;
		} else {
			logger.error("Unsupported mode: " + args[1]);
			return;
		}

		if (args.length > 2) {
			targetedBg = args[2];
		}

		client = MAMIClientFactory.build(gatewayUrl);

		bgInstanceSlice = gatherReplicaTargets(client);

		if (targetedBg == null) {
			for (Entry<String, Map<String, List<Integer>>> entryBg : bgInstanceSlice.entrySet()) {
				manageBuildGroup(client, entryBg.getKey(), entryBg.getValue(), mode);
			}
		} else {
			manageBuildGroup(client, targetedBg, bgInstanceSlice.get(targetedBg), mode);
		}
	}

	public static void manageBuildGroup(MAMIClient client, String bgName, Map<String, List<Integer>> bgInstanceSlice,
			Mode mode) {
		for (Entry<String, List<Integer>> entryInstance : bgInstanceSlice.entrySet()) {
			for (Integer indexSlice : entryInstance.getValue()) {
				switch (mode) {
				case ATTACH:
					logger.debug("ATTACH slice bg [" + bgName + "] instance [" + entryInstance.getKey() + "] slice ["
							+ indexSlice + "]");
					attachSlice(client, bgName, entryInstance.getKey(), indexSlice);
					break;
				case DETACH:
					logger.debug("DETACH slice bg [" + bgName + "] instance [" + entryInstance.getKey() + "] slice ["
							+ indexSlice + "]");
					detachSlice(client, bgName, entryInstance.getKey(), indexSlice);
					break;
				case CLEARREPLICA:
					logger.debug("CLEARREPLICA slice bg [" + bgName + "] instance [" + entryInstance.getKey()
							+ "] slice [" + indexSlice + "]");
					clearReplica(client, bgName, entryInstance.getKey(), indexSlice);
					break;
				default:
					logger.error("Unsupported mode: " + mode);
					break;
				}

				// Lets wait 2 seconds between attach / detach
				try {
					Thread.sleep(SLEEPTIME);
				} catch (InterruptedException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	public static boolean areSlicesDetached(Map<String, Map<String, List<Integer>>> bgInstanceSlice,
			List<String> buildgroups, MAMIClient client) {
		for (String bgName : buildgroups) {
			logger.debug("Check detached replicas for BG [" + bgName + "]");

			Map<String, List<Integer>> slaveSlices = bgInstanceSlice.get(bgName);

			GetBuildGroupStatus gbs = new GetBuildGroupStatus();
			gbs.setBuildGroup(bgName);

			BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

			for (IndexSliceInstanceStatus isbs : bgs.getIndexSliceInstanceStatus()) {
				if (slaveSlices.containsKey(isbs.getSliceInstance())
						&& slaveSlices.get(isbs.getSliceInstance()).contains(isbs.getIndexSlice())
						&& isbs.isReplicationAttached()) {
					logger.debug("Index slice instance [" + isbs.getSliceInstance() + "] slice ["
							+ isbs.getIndexSlice() + "] attached");
					return false;
				} else {
					logger.debug("Index slice instance [" + isbs.getSliceInstance() + "] slice ["
							+ isbs.getIndexSlice() + "] detached or not to be checked");
				}
			}
		}

		return true;
	}

	public static boolean areSlicesAttached(Map<String, Map<String, List<Integer>>> bgInstanceSlice,
			List<String> buildgroups, MAMIClient client) {
		for (String bgName : buildgroups) {
			logger.debug("Check attached replicas for BG [" + bgName + "]");

			Map<String, List<Integer>> slaveSlices = bgInstanceSlice.get(bgName);

			GetBuildGroupStatus gbs = new GetBuildGroupStatus();
			gbs.setBuildGroup(bgName);

			BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

			for (IndexSliceInstanceStatus isbs : bgs.getIndexSliceInstanceStatus()) {
				if (slaveSlices.containsKey(isbs.getSliceInstance())
						&& slaveSlices.get(isbs.getSliceInstance()).contains(isbs.getIndexSlice())
						&& !isbs.isReplicationAttached()) {
					logger.debug("Index slice instance [" + isbs.getSliceInstance() + "] slice ["
							+ isbs.getIndexSlice() + "] detached");
					return false;
				} else {
					logger.debug("Index slice instance [" + isbs.getSliceInstance() + "] slice ["
							+ isbs.getIndexSlice() + "] attached or not to be checked");
				}
			}
		}

		return true;
	}

	public static boolean areSlicesCleaned(Map<String, Map<String, List<Integer>>> bgInstanceSlice,
			List<String> buildgroups, MAMIClient client) {
		for (String bgName : buildgroups) {
			logger.debug("Check cleaned replicas for BG [" + bgName + "]");

			Map<String, List<Integer>> slaveSlices = bgInstanceSlice.get(bgName);

			GetBuildGroupStatus gbs = new GetBuildGroupStatus();
			gbs.setBuildGroup(bgName);

			BuildGroupStatus bgs = client.getManagers().getIndexingManager().getBuildGroupStatus(gbs);

			for (IndexSliceInstanceStatus isbs : bgs.getIndexSliceInstanceStatus()) {
				if (slaveSlices.containsKey(isbs.getSliceInstance())
						&& slaveSlices.get(isbs.getSliceInstance()).contains(isbs.getIndexSlice())
						&& isbs.getSerial() != -1) {
					logger.debug("Index slice instance [" + isbs.getSliceInstance() + "] slice ["
							+ isbs.getIndexSlice() + "] not cleaned");
					return false;
				} else {
					logger.debug("Index slice instance [" + isbs.getSliceInstance() + "] slice ["
							+ isbs.getIndexSlice() + "] cleaned or not to be checked");
				}
			}
		}

		return true;
	}

	public static void attachSlice(MAMIClient client, String bg, String instance, int indexSlice) {
		AttachSliceReplica replica = new AttachSliceReplica();
		replica.setBuildGroup(bg);
		replica.setSliceInstance(instance);
		replica.setIndexSlice(indexSlice);

		logger.debug("Trying to attach replica: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
				+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]");

		// TODO manage properly error
		try {
			client.getManagers().getIndexingManager().attachSliceReplica(replica);
			logger.info("Attached replica: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
					+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]");
		} catch (Exception e) {
			logger.warn(
					"Attach replica FAILED: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
							+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]", e);
		}
	}

	public static void detachSlice(MAMIClient client, String bg, String instance, int indexSlice) {
		DetachSliceReplica replica = new DetachSliceReplica();
		replica.setBuildGroup(bg);
		replica.setSliceInstance(instance);
		replica.setIndexSlice(indexSlice);

		logger.debug("Trying to detach replica: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
				+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]");

		// TODO manage properly error
		try {
			client.getManagers().getIndexingManager().detachSliceReplica(replica);
			logger.info("Detached replica: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
					+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]");
		} catch (Exception e) {
			logger.warn(
					"Detach replica FAILED: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
							+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]", e);
		}
	}

	public static void clearReplica(MAMIClient client, String bg, String instance, int indexSlice) {
		ClearSliceReplica replica = new ClearSliceReplica();
		replica.setBuildGroup(bg);
		replica.setSliceInstance(instance);
		replica.setIndexSlice(indexSlice);

		logger.debug("Trying to clear replica: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
				+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]");

		// TODO manage properly error
		try {
			client.getManagers().getIndexingManager().clearSliceReplica(replica);
			logger.info("Cleared replica: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
					+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]");
		} catch (Exception e) {
			logger.warn(
					"Clear replica FAILED: Buildgroup [" + replica.getBuildGroup() + "] Instance: ["
							+ replica.getSliceInstance() + "] Num: [" + replica.getIndexSlice() + "]", e);
		}
	}

	/**
	 * Hard work to identify attachable / detachable slices WTF ???!!!!
	 * 
	 * @param client
	 */
	public static Map<String, Map<String, List<Integer>>> gatherReplicaTargets(MAMIClient client) {
		DeploymentConfig conf = null;
		Map<String, Integer> nbSlicePerBg = null;
		Map<String, Map<String, List<Integer>>> bgInstanceSlice = null;

		// Step 1 Iterate through instance which doesn't share datadir
		// Step 2 Add couple
		// Step 3 if slice num isn't valued => all slices of the BG ...

		conf = client.getManagers().getDeploymentManager().getDeploymentConfig();

		nbSlicePerBg = new HashMap<String, Integer>();
		bgInstanceSlice = new HashMap<String, Map<String, List<Integer>>>();

		for (Host host : conf.getHost()) {
			for (Role role : host.getRole()) {
				if ("Indexing".equalsIgnoreCase(role.getName())) {
					// Getroleattrbyname a failli march� ...
					// String bgname =
					// role.getRoleAttributeByName("buildGroup").getValue();

					String bgname = null;
					int nbSlice = 0;

					for (RoleAttribute attr : role.getRoleAttribute()) {
						if ("buildGroup".equalsIgnoreCase(attr.getName())) {
							bgname = attr.getValue();
						} else if ("nbSlices".equalsIgnoreCase(attr.getName())) {
							nbSlice = Integer.valueOf(attr.getValue());
						}
					}

					nbSlicePerBg.put(bgname, nbSlice);
				}
			}
		}

		for (Host host : conf.getHost()) {
			for (Role role : host.getRole()) {
				if ("Index".equalsIgnoreCase(role.getName())) {
					String instance = null;
					String bgname = null;
					boolean useSharedDir = false;
					int sliceNum = -1;

					for (RoleAttribute attr : role.getRoleAttribute()) {
						if ("instance".equalsIgnoreCase(attr.getName())) {
							instance = attr.getValue();
						} else if ("buildGroup".equalsIgnoreCase(attr.getName())) {
							bgname = attr.getValue();
						} else if ("useSharedDir".equalsIgnoreCase(attr.getName())) {
							useSharedDir = Boolean.valueOf(attr.getValue());
						} else if ("indexSlice".equalsIgnoreCase(attr.getName())) {
							sliceNum = Integer.valueOf(attr.getValue());
						}
					}

					if (useSharedDir) {
						continue;
					}

					if (!bgInstanceSlice.containsKey(bgname)) {
						bgInstanceSlice.put(bgname, new HashMap<String, List<Integer>>());
					}

					Map<String, List<Integer>> tmpMap = bgInstanceSlice.get(bgname);

					if (!tmpMap.containsKey(instance)) {
						tmpMap.put(instance, new ArrayList<Integer>());
					}

					// If num slice is filed => it's not all bg slices
					if (sliceNum != -1) {
						tmpMap.get(instance).add(sliceNum);
					} else {
						for (int i = 0; i < nbSlicePerBg.get(bgname); i++) {
							tmpMap.get(instance).add(i);
						}
					}
				}
			}
		}

		return bgInstanceSlice;
	}
}
